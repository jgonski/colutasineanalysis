###################################################################################################################
#
#  This script contains plotting functions used by PlotMaster.py
#   - Plot Fitted Sine
#   - Plot Sine Residuals
#   - Check Sine Fits: checks previously calculated sine fit parameters (by plotting them) -- manual check
#   - Plot FFT
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#  Updated: May 2019
#  
###################################################################################################################

import sys
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.fftpack import fft

# Local Modules
sys.path.append("../src/")
import SineFunctions as sf
from SineWave import SineWave

#------------------------------------------------------------------------------------------------------------------
def plotResidualHists( MySineWaveResidual, gain_arr, name ):
   arr1 = []
   arr2 = []
   arr3 = []
   arr4 = []
   arr5 = []
   arr6 = []
   arr7 = []
   #hist1 = TH1F()
   #hist2 = TH1F()
   #hist3 = TH1F()
   #hist4 = TH1F()
   #hist5 = TH1F()
   #hist6 = TH1F()
   
   for index in range(len(gain_arr)-1):
     #print('gain arr: ' , gain_arr[index])
     if gain_arr[index] == 7: arr7.append(MySineWaveResidual.sine[index])#hist6.Fill(MySineWaveResidual.sine[index]) # 
     if gain_arr[index] == 6: arr6.append(MySineWaveResidual.sine[index])#hist6.Fill(MySineWaveResidual.sine[index]) # 
     if gain_arr[index] == 5: arr5.append(MySineWaveResidual.sine[index])#hist5.Fill(MySineWaveResidual.sine[index]) # 
     if gain_arr[index] == 4: arr4.append(MySineWaveResidual.sine[index])#hist4.Fill(MySineWaveResidual.sine[index]) # 
     if gain_arr[index] == 3: arr3.append(MySineWaveResidual.sine[index])#hist3.Fill(MySineWaveResidual.sine[index]) # 
     if gain_arr[index] == 2: arr2.append(MySineWaveResidual.sine[index])#hist2.Fill(MySineWaveResidual.sine[index]) # 
     if gain_arr[index] == 1: arr1.append(MySineWaveResidual.sine[index])#hist1.Fill(MySineWaveResidual.sine[index]) # 
  
   #hist1.Fit('gaus')
   #f1 = hist1.GetFunction("gaus")
   #m2 = f1.GetParameter(0)
   #hist2.Fit('gaus')
   #f2 = hist2.GetFunction("gaus")
   #m3 = f2.GetParameter(0)
   #hist3.Fit('gaus')
   #f3 = hist3.GetFunction("gaus")
   #m4 = f3.GetParameter(0)
   #hist4.Fit('gaus')
   #f4 = hist4.GetFunction("gaus")
   #m5 = f4.GetParameter(0)
   #hist5.Fit('gaus')
   #f5 = hist5.GetFunction("gaus")
   #m6 = f5.GetParameter(0)
   #hist6.Fit('gaus')
   #f6 = hist6.GetFunction("gaus")
   #m7 = f6.GetParameter(0)

   #print('MDAC Bit 2: ', m2)
   #print('MDAC Bit 3: ', m3)
   #print('MDAC Bit 4: ', m4)
   #print('MDAC Bit 5: ', m5)
   #print('MDAC Bit 6: ', m6)
   #print('MDAC Bit 7: ', m7)

   plt.hist(arr7,label='MDAC Bit 7')
   plt.hist(arr6,label='MDAC Bit 6')
   plt.hist(arr5,label='MDAC Bit 5')
   plt.hist(arr4,label='MDAC Bit 4')
   plt.hist(arr3,label='MDAC Bit 3')
   plt.hist(arr2,label='MDAC Bit 2')
   plt.hist(arr1,label='MDAC Bit 1')
   plt.xlabel('Residual [ADC Counts]')
   plt.legend(loc=1, frameon=False)
   #plt.show()
   plt.savefig('../results/Residuals/hist_residuals_'+name.split('/')[3].split('.')[0]+'.pdf')
   print('Saved fig: ', '../results/Residuals/hist_residuals_'+name.split('/')[3].split('.')[0]+'.pdf')

   plt.clf()
   plt.cla()
   plt.close()

#------------------------------------------------------------------------------------------------------------------
def plotFittedSine( MySineWave, doCalculation=True, datastyle='b.', fitstyle='b-', legend='FitInfo', Nsamples=None ):
    """ Plots the fitted sine
    Inputs: SineWave: sine wave object
            SineWavePlotting: sine wave plotting parameters
    """

    fit_params_tmp = None

    # Check if fit calculation done
    if doCalculation is True:
        if MySineWave.fit_params is None: 
            MySineWave.FitSine()
        fit_params_tmp = MySineWave.fit_params

    if doCalculation is False:
        if MySineWave.prevfit_params is None:
            #print "Error plotFittedSine(): Previously calculated sine wave parameters not properly loaded."
            return
        fit_params_tmp = MySineWave.prevfit_params

    # Points to plot 
    nplot = Nsamples
    if Nsamples is None:
        nplot = int(4/float( fit_params_tmp[0])) + 50 # 4 full sines + some more
    nplot = 400

    x = np.arange(nplot)
    nfine = nplot*10
    xfine = np.arange(nplot,step=float(nplot)/float(nfine))

    # Get Fit Information
    if 'FitInfo' in legend:
        freqstr  = str(round(fit_params_tmp[0]*float(MySineWave.sample), 3))
        ampstr   = str(round(fit_params_tmp[1], -1))
        phasestr = str(round(fit_params_tmp[2],  2))
        offsetstr = str(round(fit_params_tmp[3],  1)) 

        FitInfo  = 'FIT: freq='+freqstr+', amp='+ampstr+',\n' 
        FitInfo += '      phase='+phasestr+', offset='+offsetstr

    else:
        FitInfo = ''
          
    # Plot data
    plt.plot(x, MySineWave.sine[:nplot], datastyle, label=legend.replace('FitInfo', '' ))

    # Plot finely sampled fitted sine
    fitted_sine_fine = sf.mySine(xfine, *fit_params_tmp)
    plt.plot(xfine, fitted_sine_fine, fitstyle, label=FitInfo)
    
    # Format/Save/Show/Legend - outside of this function

#------------------------------------------------------------------------------------------------------------------
def plotSineResiduals( MySineWave, datastyle='b.', fitstyle='b-', legend1='FitInfo', legend2='FitInfo', Nsamples=None):
    """ Plots the fitted sine, and plots the residual (with the fit to the residual) """


    # Check if fit calculation done
    if MySineWave.fit_params is None:
        MySineWave.FitSine()

    # Residual
    MySineWaveResidual = MySineWave.GetSineWaveResidual()
    MySineWaveResidual.FitSine()

    # Corrected
    MySineWaveCorrected = MySineWave.GetSineWaveCorrected()
    MySineWaveCorrected.FitSine()

    legend1 = legend1.replace('freq', 'fit freq='+str(round(MySineWave.freq_fit_MHz, 3)))
    legend2 = legend2.replace('freq', 'fit freq='+str(round(MySineWaveResidual.freq_fit_MHz, 3)))
	                
    # Plot
    plt.subplot(211)
    plotFittedSine( MySineWave, True, datastyle, fitstyle, legend1, Nsamples )

    plt.subplot(212)
    plotFittedSine( MySineWaveResidual, True, datastyle, fitstyle, legend2, Nsamples )

    # Format/Save/Show/Legend - On your own, after this function is called.

#------------------------------------------------------------------------------------------------------------------
def checkSineFit( MySineWave ):
    """ A manual check on the sine fits of the fit parameters that were stored using ExtractSinePerformance.py """

    if MySineWave.sinedata_isOK == False:
        #print "Error checkSineFit(): Previously calculated sine wave parameters not properly loaded."
        return

    MySineWaveResidual = MySineWave.GetSineWaveResidual()
    MySineWaveResidual.prevfit_params = MySineWave.prevfitR_params

    # Plot
    plt.subplot(211)
    plotFittedSine( MySineWave, doCalculation=False, datastyle='b.-', fitstyle='b-', legend='FitInfo' )
    plt.title('Sine Fit')
    plt.ylabel('ADC Counts [Sine]')
    plt.legend(loc='upper right', fancybox=True, fontsize='10')

    plt.subplot(212)
    plotFittedSine( MySineWaveResidual, doCalculation=False, datastyle='b.-', fitstyle='b-', legend='FitInfo' )
    plt.ylabel('ADC Counts [Residual]'), plt.xlabel('Sample Number')
    plt.legend(loc='upper right', fancybox=True, fontsize='10')

    # Format/Save/Show - On your own, after this function is called.

#------------------------------------------------------------------------------------------------------------------
def plotFFT( MySineWave, plotstyle='b.-', legend='', plot_dB=True):
    """ Plots FFTs """

    fft_w = MySineWave.CalculateFFT(useWindow=True)
    		
    # ADC Performance measurements
    MySineWave.CalculatePerformanceParams()

    enob_val  = MySineWave.CalculateENOB()
    sfdr_val  = MySineWave.CalculateSFDR()
    snr_val   = MySineWave.CalculateSNR()

    legend += ': ENOB=' + str(round(enob_val  , 2))
    legend += ', SFDR=' + str(round(sfdr_val  , 0))
    legend += ', SNR='+ str(round(snr_val , 0))

    # FFT Params
    N   = len(fft_w)
    T   = (1./float(MySineWave.sample))*1e-6
    x   = np.linspace(0.0, N*T, N) 
    xf  = np.linspace(0.0, 1.0/(2.0*T), N/2) / 1.0e6 # plot in MHz
    y   = 2.0/N * np.abs(fft_w[0:int(N/2)])
		
    ymax = max(y[5:]) # exclude dc
    ydB = [ 20*math.log( float(f/ymax), 10 ) for f in y ] 
		
    if plot_dB:
        plt.plot(xf, ydB, plotstyle, label=legend) 
    else:
        plt.plot(xf, y  , plotstyle, label=legend)
        plt.semilogy()

    plt.title('FFT')
    plt.xlabel('Frequency (MHz)')
    plt.ylabel('ADC Counts')
    plt.legend(loc='upper right', fancybox=True, fontsize='10')
    plt.xticks(np.arange(min(xf), max(xf)+1, (max(xf)-min(xf))/10.0 ))
    plt.grid(axis='both')

    # Format/Save/Show - On your own, after this function is called.
