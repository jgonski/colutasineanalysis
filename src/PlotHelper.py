#####################################################################################
#
#  This file provides supporting functions for saving and styling plots and
#  information accociated with them.
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#  Updated: May 2019
#  
#####################################################################################

#------------------------------------------------------------------------------------
def plotpath(plottype,  gain, freq, amp, sample, idx, compare=None):
    """ Constructs paths for plots. Note that plottype includes plot path as well """
    
    if compare == 'gain' : gain = 'gainAll'
    if compare == 'freq' : freq = 'All'
    if compare == 'amp'  : amp  = 'All'
    if compare == 'index': idx = 'idxAll'
	
    if 'special' in compare:
        path = plottype+'_freq'+freq+'_'+sample+'MSPS_'+compare+'_'+idx+'.png'
    else:
        path = plottype+'_freq'+freq+'_amp'+amp+'_'+gain+'_'+sample+'MSPS_'+idx+'.png'
        
    return path

#------------------------------------------------------------------------------------
def plotInfo(gain, freq, amp, sample, idx, compare):

    if compare == 'gain' : gain = 'gainAll'
    if compare == 'freq' : freq = 'All'
    if compare == 'amp'  : amp  = 'All'
    if compare == 'index': idx = 'idxAll'
    if 'special' in compare:
        string = 'freq'+freq+' '+sample+'MSPS '+idx
        return string

    string = 'freq='+freq+' amp='+amp+' '+gain+' '+sample+'MSPS '+idx        
    return string

#------------------------------------------------------------------------------------
def plotLegend(gain, freq, amp, sample, idx, compare):

    legend = ''
    if compare == 'gain' : legend = gain
    if compare == 'freq' : legend = freq
    if compare == 'amp'  : legend = amp
    if 'special' in compare: legend += gain + ' ' + amp
    if compare == 'index': legend = ''

    return legend

#------------------------------------------------------------------------------------
def plotStyle(i,linestyle=None, colorstyle=None):
        
    plot_style = ['b.-', 'r.-', 'g.-', 'm.-', 'c.-', 'y.-',
                  'b+-', 'r+-', 'g+-', 'm+-', 'c+-', 'y+-',
                  'bx-', 'rx-', 'gx-', 'mx-', 'cx-', 'yx-',
                  'bo-', 'ro-', 'go-', 'mo-', 'co-', 'yo-']
        
    if colorstyle == 'rainbow':
        plot_style = ['k.-', 'r.-', 'b.-', 'm.-', 'g.-', 'c.-', 'y.-', 
                      'k+-', 'r+-', 'b+-', 'm+-', 'g+-', 'c+-', 'y+-', 
                      'kx-', 'rx-', 'bx-', 'mx-', 'gx-', 'cx-', 'yx-', 
                      'ko-', 'ro-', 'bo-', 'mo-', 'go-', 'co-', 'yo-' ]

    i = i%(len(plot_style))

    if linestyle == 'lines':
        plot_style[i] = plot_style[i].replace('.','')
        plot_style[i] = plot_style[i].replace('+','')
        plot_style[i] = plot_style[i].replace('x','')
        plot_style[i] = plot_style[i].replace('o','')

    if linestyle == 'symbols':
        plot_style[i] = plot_style[i].replace('-','')

    if linestyle == 'colors':
        plot_style = ['b', 'r', 'g', 'm', 'c', 'y', 'orange', 'lightgreen', 'k']
        i = i%(len(plot_style))
	
    return plot_style[i]
