#####################################################################################
#
#  This file contains the definitions of the SineWave class and the AutogainSineWave
#  class, which inherits from SineWave. These classes handle sine wave data as 
#  objects and include member functions used for sine wave fitting, sine wave 
#  residual calculation, sine wave performance parameter calculation, autogain 
#  calibrations and corrections, and more. 
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: May 2019
#  
#####################################################################################


import math
import numpy as np
from scipy.fftpack import fft
from scipy.optimize import curve_fit
from scipy import stats

# User-defined functions
import SineFunctions as sf

#
# <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><
# <> SineWave Class <>
# <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><
#
class SineWave:
    def __init__(self, gain, freq, amp, sampling_frequency, index,
                 adc_counts, gain_bit, debug):
        
        self.Reset()

        self.gain   = gain
        self.freq   = freq
        self.amp    = amp
        self.sample = sampling_frequency
        self.index  = index
        self.debug  = debug

        self.adc_counts = adc_counts
        self.gain_bit   = gain_bit
        self.N          = len(adc_counts)

        # For the fit
        self.max_diff_cutoff = 0.05

        # Initialize class values 

        if self.gain in ['1x', '4x']:
            self.sine = adc_counts

    #
    # ===============================================================================
    # Sine Functions #
    # ===============================================================================
    #

    # -------------------------------------------------------------------------------
    def WindowMe(self):
        """Returns sine wave passed through a windowing function, typically before
        calculating fft
        """
        if self.sine is None:
            return

        self.isWindowed = True
        self.window = "blackman"
        self.sine_w = self.sine*np.blackman(self.N)

        return self.sine_w

    # -------------------------------------------------------------------------------
    def CalculateFFT(self, useWindow=True):
        """Returns fft of sine wave, calculated from scipy.fftpack.fft() function."""

        if self.sine is None:
            return None

        if useWindow:
            if self.sine_w is None: 
                self.WindowMe()
            self.fft = fft(self.sine_w)
        else:
            self.fft = fft(self.sine)

        return self.fft

    # -------------------------------------------------------------------------------
    def CalculateSINAD(self):
        """Calculate the signal-to-noise and distortion ratio"""

        if self.fft is None:
            self.CalculateFFT()

        n_incl = 5 # bins to include in fundamental and exclude DC

        fft = self.fft[n_incl:int(self.N/2)] 

        fmax = np.argmax( fft ) 
    
        P_fund = sum( abs(f)*abs(f) for f in fft[fmax-n_incl:fmax+n_incl+1] )
        P_nd   = sum( abs(f)*abs(f) for f in fft ) - P_fund

        self.sinad = 10*math.log( P_fund / P_nd , 10 )
        return self.sinad

    # -------------------------------------------------------------------------------
    def CalculateENOB(self):
        """Calculate the eff4ective number of bits"""
        if self.sinad is None:
            self.CalculateSINAD()

        self.enob = ( self.sinad - 1.76 ) / 6.02 #( self.sinad - 1.76 + corr ) / 6.02
        return self.enob

    # -------------------------------------------------------------------------------
    def CalculateSFDR(self):
        """Calculate the spurrious free dynamic range."""
        if self.fft is None:
            self.CalculateFFT()

        n_incl = 5 # bins to include in fundamental and exclude DC
        fft = self.fft[n_incl:int(self.N/2)]

        fmax = np.argmax( fft )

        # fundamental max
        fmaxval1 = max( abs(fft) ) #fundamental max

        # maximum spur/harmonic
        fmaxval2 = max( max(abs( fft[ 1 : fmax-n_incl ] )), 
                   max(abs( fft[ fmax+n_incl+1 : ] )) )

        self.sfdr = 20*math.log( abs(fmaxval1) / abs(fmaxval2) , 10)

        return self.sfdr

    # -------------------------------------------------------------------------------
    def CalculateSNR(self):
        """Calculates signal to noise ratio."""
        #if self.debug: 
        #    print("Calculating SNR...")

        n_incl = 5

        fft = self.fft[n_incl:int(self.N/2)]

        # get median of middle quartile -- noise floor
        n_i = len(fft)*3/8
        n_f = len(fft)*5/8
        fft_sorted = np.sort(abs(fft))
    
        signal = max(abs(fft))
        noise  = np.mean( fft_sorted[n_i:n_f] )

        self.snr = 20*math.log(signal/noise, 10) # in db

        return self.snr 

    # -------------------------------------------------------------------------------
    def CalculatePerformanceParams(self):
        """Set sine performance parameters"""
        self.WindowMe()
        self.CalculateFFT()
        self.CalculateSINAD()
        self.CalculateENOB()
        self.CalculateSFDR()
        self.CalculateSNR()

    #
    # ===============================================================================
    # Sine Fitting #
    # ===============================================================================
    #

    # -------------------------------------------------------------------------------
    def EstimateFreq(self):
        """Estimates the frequency in *SAMPLE* space"""

        if self.fft is None:
            self.CalculateFFT()

        self.freq_est = ( np.argmax(abs( self.fft[5:int(self.N/2)] ) ) + 5 )/float(self.N)  # return index in fft
        return self.freq_est

    # -------------------------------------------------------------------------------
    def EstimateAmp(self):
        """Estimates the amplitude in adc counts"""
        self.amp_est = ( max(self.sine[20:]) - min(self.sine[20:]) ) / 2.0
        return self.amp_est

    # -------------------------------------------------------------------------------
    def EstimatePhase(self):
        """Estimates the phase of the sine wave in radians"""

        if self.freq_est is None:
            self.estimateFreq()

        n_per_sin = 1./self.freq_est

        phase_est = (np.argmax(self.sine)*2.*math.pi/n_per_sin)%(2.*math.pi) - math.pi/2.
        phase_est = sf.fixPhase(phase_est) # Put phase in range [0, 2pi)

        self.phase_est = phase_est
        return self.phase_est

    # -------------------------------------------------------------------------------
    def EstimateOffset(self):
        """Estimates the vertical offset/shift of the sine wave"""
        self.offset_est = np.mean(self.sine)
        return self.offset_est

    # -------------------------------------------------------------------------------
    def FitSine(self):
        """Fits a sine wave. If it doesn't find a fit (based on the max_diff_cutoff param),
        it "wiggles" the fit around until it finds one. If the number of fit iterations
        reaches 12, it returns the fit is has, with a warning statement."""

        #if self.debug: 
        #    print("Fitting Sine...")

        # Calculate Estimates #

        freq_est   = self.EstimateFreq() 
        amp_est    = self.EstimateAmp()
        phase_est  = self.EstimatePhase()
        offset_est = self.EstimateOffset()

        p0 = [freq_est, amp_est, phase_est, offset_est]
        #if self.debug: print("Fit Guess :", p0)

        # Fit #
        x   = np.arange(self.N)
        fit = curve_fit(sf.mySine, x, self.sine, p0=p0)
        fitted_sine = sf.mySine(x, *fit[0])

        # If fit wrong, wiggle fit values and try again
        #if self.debug: print("First Fit Attempt:",fit[0])
        
        max_diff_cutoff = self.max_diff_cutoff
        max_diff_val = sf.maxDiff(self.sine, fitted_sine)/float(amp_est)
        
        i = 1
        while max_diff_val > max_diff_cutoff:
            i+= 1
                    
            rand = [np.random.uniform(0.995, 1.005 ),
                    np.random.uniform(0.9  , 1.1   ),
                    np.random.uniform(0.95 , 1.05  ),
                    np.random.uniform(0.995, 1.005 )]

            newfit = fit[0]*rand
            newfit[2] += np.random.uniform(-0.5,0.5)
                    
            fit = curve_fit(sf.mySine, x, self.sine, newfit)
            fitted_sine = sf.mySine(x, *fit[0])

            max_diff_val = sf.maxDiff(self.sine, fitted_sine)/float(amp_est)

            # Only extreme debugging cases
            #if self.debug: print 'Fit Iteration', i+1, ':', newfit, ' chi2=', sf.chi2(self.sine, fitted_sine), ' maxDiff=', sf.maxDiff(self.sine, fitted_sine)/amp_est
                    
            if i%3 == 0 :
                fit[0][0],fit[0][1],fit[0][2],fit[0][3] = p0 # reset occasionally
                    
            if i > 12 :
                #print 'WARNING: cannot find fit after 12 additional iterations. Fit is not OK.'
                break
            
        # Correct negative amplitudes and phase
        if fit[0][1] < 0:
            fit[0][1]  = abs(fit[0][1])
            fit[0][2] += math.pi
        
        # Fix the phase
        fit[0][2] = sf.fixPhase( fit[0][2] )

        #if self.debug: 
        #    print "FIT PARAMS ( number of iterations:", i, "), chi2 =",  sf.chi2(self.sine, fitted_sine)
        #    print "\t First Guess: \t\tFitted: "
        #    print "\tfreq  = ", round(freq_est*float(self.sample) , 3), "   \t=", round(fit[0][0]*float(self.sample), 3), "MHz" #in MHz
        #    print "\tamp   = ", round(amp_est  , 0), "   \t=", round(fit[0][1], 3)
        #    print "\tphase = ", round(phase_est, 1), "\t\t=" , round(fit[0][2], 3)
        #    print "\toffset = ", round(offset_est, 2), "\t="   , round(fit[0][3], 3)

        self.fit_max_diff = max_diff_val

        if max_diff_val < max_diff_cutoff: 
            self.fit_isOK = True

        self.fitted_sine = fitted_sine
        self.freq_fit    = fit[0][0]
        self.amp_fit     = fit[0][1]
        self.phase_fit   = fit[0][2]
        self.offset_fit  = fit[0][3]
        self.fit_params  = [self.freq_fit, self.amp_fit, self.phase_fit, self.offset_fit]
        self.freq_fit_MHz = fit[0][0]*float(self.sample) # in MHz            


    # -------------------------------------------------------------------------------
    def GetSineWaveResidual(self):
        """Get the sine wave residual from the fit -- is it sinusoidal at a different 
        frequency, indicative of harmonic distortion?"""

        if self.fit_params is None:
            self.FitSine()

        residual = self.sine - self.fitted_sine
        self.SineWaveResidual = SineWave(self.gain, self.freq, self.amp, self.sample, self.index, 
                                         residual, self.gain_bit, self.debug)

        return self.SineWaveResidual

    # -------------------------------------------------------------------------------
    def GetSineWaveCorrected(self):
        """Get the sine wave corrected from the residual fit -- does this improve 
        the ENOB? """

        if self.fit_params is None:
            self.FitSine()

        if self.SineWaveResidual is None:
            self.GetSineWaveResidual(self)

        if self.SineWaveResidual.fit_params is None:
            self.SineWaveResidual.FitSine()

        corrected = self.sine - self.SineWaveResidual.fitted_sine
        self.SineWaveCorrected = SineWave(self.gain, self.freq, self.amp, self.sample, self.index, 
                                          corrected, self.gain_bit, self.debug)

        return self.SineWaveCorrected 

    #
    # ===============================================================================
    # Load previously calculated sine wave data #
    # ===============================================================================
    #

    # -------------------------------------------------------------------------------
    def LoadSineData(self, sinedata_path):

        self.sinedata_path = sinedata_path
        self.sinedata = np.load( sinedata_path ).item()
        self.sinedata_isOK = True

        gain, freq, amp = self.gain, self.freq, self.amp

        # Read in previously calculated parameters
        try:
            freq_prevfit   = np.mean(  self.sinedata[( gain, freq, amp)][ 'freq'   ] )
            amp_prevfit    = np.mean(  self.sinedata[( gain, freq, amp)][ 'amp'    ] )     
            phase_prevfit  = sf.avgPhase( self.sinedata[( gain, freq, amp)][ 'phase'  ] )[0]
            offset_prevfit = np.mean(  self.sinedata[( gain, freq, amp)][ 'offset' ] )
            self.prevfit_params = [freq_prevfit, amp_prevfit, phase_prevfit, offset_prevfit]
        except KeyError:
            #print "WARNING SineWave.LoadSineData(): Unable to load some parameters from",sinedata_path
            #print "  These params may include: freq, amp, phase, offset"
            self.sinedata_isOK = False

        # Residual parameters
        try:
            freq_prevfit   = np.mean(  self.sinedata[( gain, freq, amp)][ 'freqR'   ] )
            amp_prevfit    = np.mean(  self.sinedata[( gain, freq, amp)][ 'ampR'    ] )     
            phase_prevfit  = sf.avgPhase( self.sinedata[( gain, freq, amp)][ 'phaseR'  ] )[0]
            offset_prevfit = np.mean(  self.sinedata[( gain, freq, amp)][ 'offsetR' ] )
            self.prevfitR_params = [freq_prevfit, amp_prevfit, phase_prevfit, offset_prevfit]
        except KeyError:
            #print "WARNING SineWave.LoadSineData(): Unable to load some parameters from",sinedata_path
            #print "  These params may include: freqR, ampR, phaseR, offsetR"
            self.sinedata_isOK = False

        # Corrected parameters
        try:
            freq_prevfit   = np.mean(  self.sinedata[( gain, freq, amp)][ 'freqC'   ] )
            amp_prevfit    = np.mean(  self.sinedata[( gain, freq, amp)][ 'ampC'    ] )     
            phase_prevfit  = sf.avgPhase( self.sinedata[( gain, freq, amp)][ 'phaseC'  ] )[0]
            offset_prevfit = np.mean(  self.sinedata[( gain, freq, amp)][ 'offsetC' ] )
            self.prevfitC_params = [freq_prevfit, amp_prevfit, phase_prevfit, offset_prevfit]
        except KeyError:
            #print "WARNING SineWave.LoadSineData(): Unable to load some parameters from",sinedata_path
            #print "  These params may include: freqC, ampC, phaseC, offsetC"
            self.sinedata_isOK = False

    #
    # -------------------------------------------------------------------------------
    # Reset #
    # -------------------------------------------------------------------------------
    #

    # -------------------------------------------------------------------------------
    def Reset(self):
        """Resets parameters to initialized values"""

        # First to be initialized ---------------------------

        self.freq   = None
        self.amp    = None
        self.gain   = None
        self.sample = None
        self.index  = None
        self.debug  = None
        self.adc_counts = None
        self.gain_bit   = None
        self.N          = None

        self.max_diff_cutoff = None

        self.sine = None

        # Sine Wave Params & FFT-related --------------------
        self.isWindowed = False
        self.window     = None
        self.sine_w     = None
        self.fft   = None
        self.sinad = None
        self.enob  = None
        self.sfdr  = None

        # Fit-Related ---------------------------------------
        self.fit_isOK     = False
        self.fit_max_diff = None
        self.fitted_sine  = None
        self.fit_params   = None

        self.freq_est   = None
        self.amp_est    = None
        self.phase_est  = None
        self.offset_est = None

        self.freq_fit   = None
        self.amp_fit    = None
        self.phase_fit  = None
        self.offset_fit = None
        self.freq_fit_MHz = None

        self.SineWaveResidual = None
        self.SineWaveCorrected = None

        # Sine Wave Data, Previously Calculated -------------
        self.sinedata_path = None
        self.sinedata      = None
        self.sinedata_isOK = False

        self.freq_prevfit   = None
        self.amp_prevfit    = None
        self.phase_prevfit  = None
        self.offset_prevfit = None
        self.prevfit_params = None

        self.freqR_prevfit   = None
        self.ampR_prevfit    = None
        self.phaseR_prevfit  = None
        self.offsetR_prevfit = None
        self.prevfitR_params = None

        self.freqC_prevfit   = None
        self.ampC_prevfit    = None
        self.phaseC_prevfit  = None
        self.offsetC_prevfit = None
        self.prevfitC_params = None

#
# <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><
# <> AutogainSineWave Class <>
# <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><
#
class AutogainSineWave(SineWave):
    def __init__(self, gain, freq, amp, sampling_frequency, index,
                 adc_counts, gain_bit, debug, sine=None):
        
        self.ResetAll()

        self.gain   = gain
        self.freq   = freq
        self.amp    = amp
        self.sample = sampling_frequency
        self.index  = index
        self.debug  = debug

        self.adc_counts = adc_counts
        self.gain_bit   = gain_bit
        self.N          = len(adc_counts)

        self.sine = sine

        # For the fit
        self.max_diff_cutoff = 0.05

    #
    # -------------------------------------------------------------------------------
    # Autogain Calibration -- TODO #
    # -------------------------------------------------------------------------------
    #

    # -------------------------------------------------------------------------------
    def SetAutogainSineParams(self, param_type, input_fit_params1x, input_fit_params4x ):
        """Set the sine data for autogain calibration"""

        # For sine fit data from 1x and 4x only
        if param_type == "sine": 
            self.prevfit_params_1x  = input_fit_params1x
            self.prevfit_params_4x  = input_fit_params4x
            self.gain_calc = input_fit_params4x[1]/input_fit_params1x[1]
            self.offset_1x = input_fit_params1x[3]
            self.offset_4x = input_fit_params4x[3]

        # For sine residual fits
        if param_type == "res" : 
            self.prevfitR_params_1x = input_fit_params1x
            self.prevfitR_params_4x = input_fit_params4x

    # -------------------------------------------------------------------------------
    def CorrectAG_Baseline( self ):
        """This function provides the baseline correction for the autogain: shifts 
        and scales data (to 14 bit scale). Shift and gain informatino previously 
        determined from 1x and 4x-only data.
        """
    
        #if None in [self.gain_calc, self.offset_1x, self.offset_4x]:
        #    print "ERROR SineWave.CorrectAG(): fit parameters from 1x and 4x samples not properly loaded."
        #    print "      --> Set these using SineWave.SetAutogainSineParams()"

        N = self.N 
        x = np.arange( N )
        
        # Shifts and Gains #
        offsets_1x = np.full(N, self.offset_1x)
        offsets_4x = np.full(N, self.offset_4x)
        offsets    = [ offsets_1x[i]*self.gain_bit[i] + offsets_4x[i]*(1.-self.gain_bit[i]) for i in range(N) ]
        gains      = [ ((self.gain_calc-1.)*g + 1. ) for g in self.gain_bit ]

        corrected = [ (self.adc_counts[i] - offsets[i])*gains[i] + 2**13 for i in range(N) ] # 2^13 is middle of the range

        self.sine = corrected

        return self.sine

    # -------------------------------------------------------------------------------
    def CorrectAG_Spikes( self, method, c01=0, c10=0 ): #sine_rawCorr, gain_bit, sine_ref, method='C', c01=0, c10=0):

        """This function corrects for the spikes we see in the autogain data when the gain bit flips. We typically see
        four different "types" of gain transitions: a) 0->1 when sine wave deriv > 0, b) 0->1 when sine wave deriv < 0,
        c) 1->0 when sine wave deriv > 0, d) 1->0 when sine wave deriv < 0. These are typically calculated and applied
        independently, except for the 'C' (constant) method, which applies a constant spike correction to 0->1 & 1->0.

        sine_ref : The referece sine wave used to determine what the spike values are (not required for method='C').
            Typically use a fitted sine wave.
        
        A number of different correction algorithms/methods are available to use. Options for 'method' variable:
            - 'S', Standard Correction (used to be called 'Z'): Calculates the spike values of the input sine wave,
                calculates the average value for the 4 types of spikes, and applies this average as the correction.
            - 'P', Percentage: Same as Standard, except it scales the correction by a constant value before applying it
                    -- Example: method='P75' scales the spike values applied to 75%. (NB: The default is 85%)
            - 'M', Mitpoint: The correction applied is one such that the spike defiation from the reference is the
                midpoint deviation of that of it's neighbors. Used to mitigate other noise and distortion factors to
                prevent over-correcting. 
            - 'D', Dynamic: Useful for when gain bit values != 0 or 1 (when sine waves are averaged), this applies the
                correction scaled by the deviation from the "closest" gain bit value.
                    -- If gain bit = 0.85, scale = 0.85; if gain bit == 0.15, scale = 0.85 
            - 'C', Constant: Apply a constant and predetermined spike correction to 0->1 and 1->0 transitions.
            
        Note that many of these gain bit methods can be used with each other.
            Examples:
            -- method='MP50' : Uses M method to calculate correction then multiplies corrs by 50% when applying them
            -- method='CD' or 'DC' : Applied constant corrections in a dynamic way (scales based on gain bit vals)
        """

        if self.fitted_sine is None:
            self.FitSine()

        #if self.debug: 
        #    print 'Function: correctAG_GTspikes(..), spike method=', method

        #------------------------------------------------------------------------------ #
        def myCorr( AGdiff, i ):
            """Returns different types of corrections"""
            # Constant (doesn't matter here)
            if 'C' in method: return 0
            # Midpoint
            if 'M' in method: return AGdiff[i] - ( AGdiff[i-1] + AGdiff[i+1] )*0.5
            # Zero 
            elif 'S' in method or 'P' in method: return AGdiff[i]
            # Dynamic
            elif 'D' in method:
                if gain_bit[i] > .5: return AGdiff[i]*gain_bit[i]
                else: return AGdiff[i]*(1 - gain_bit[i])
            # Other
            else: return 0.
        #------------------------------------------------------------------------------ #
            
        N = self.N
        x = np.arange( N )

        reference_sine = self.fitted_sine
        gain_bit       = self.gain_bit

        sine_grad = np.gradient(self.sine)
        AGdiff    = [ self.sine[i] - reference_sine[i] for i in range(N) ]

        # Initialize Arrays to Calculate Average Values #
        pos01_ = []
        pos10_ = []
        neg01_ = []
        neg10_ = []

        # Bounds to determine gain bit cutoff in the case of gain_bit !=0 , !=1 (in the case of averaging)
        bmin = 0.25
        bmax = 0.75

        # Get Spike Values
        for i in range(N):
            if i == 0 or i == N-1: continue
            if 'C' in method: break # Constant Correction --> Apply manually

            if sine_grad[i] > 0. :
                if gain_bit[i-1] < bmin and gain_bit[i] > bmax : pos01_.append( myCorr( AGdiff , i ) ) 
                if gain_bit[i-1] > bmax and gain_bit[i] < bmin : pos10_.append( myCorr( AGdiff , i ) ) 
            elif sine_grad[i] < 0. :
                if gain_bit[i-1] < bmin and gain_bit[i] > bmax : neg01_.append( myCorr( AGdiff , i ) )
                if gain_bit[i-1] > bmax and gain_bit[i] < bmin : neg10_.append( myCorr( AGdiff , i ) )

        # Fill with zeros if empty
        if len(pos01_) == 0: pos01_.append(0.)
        if len(pos10_) == 0: pos10_.append(0.)
        if len(neg01_) == 0: neg01_.append(0.)
        if len(neg10_) == 0: neg10_.append(0.)

        # Get average
        pos01 = np.mean(pos01_)
        pos10 = np.mean(pos10_)
        neg01 = np.mean(neg01_)
        neg10 = np.mean(neg10_)

        # Redefine values if method == Constant
        if 'C' in method: 
            if c01 == 0 or c10 == 0: print('WARNING: Spike Correction Constant(s) set to 0')
            pos01 = -c01
            pos10 =  c10
            neg01 = -c01
            neg10 =  c10

        # Scale correction? Optional (for tests)
        scale = 1.

        # Initialize corrected output 
        corrected = []

        # Option to scale the correction to a percentage of what it already is
        if 'P' in method: 
            Pscale = 1.0
            try: # is Pscale value defined in the 'method' argument?
                Pscale = float( method[ method.find('P') + 1: ] )*.01
            except ValueError: # Default
                Pscale = 0.85 

        # Apply Spike Values ---------------------------------------------------------- #
        for i in range(N):
            if i == 0:
                corrected.append( self.sine[i] )
                continue

            if 'D' in method: # Apply Corrections Dynamically - scale determined by gain bit value
                if gain_bit[i] > .5: scale = gain_bit[i]
                else: scale = 1 - gain_bit[i]

            if 'P' in method: scale *= Pscale
            
            if   sine_grad[i] > 0. and gain_bit[i-1] < bmin and gain_bit[i] > bmax : corrected.append( self.sine[i] - pos01*scale )
            elif sine_grad[i] > 0. and gain_bit[i-1] > bmax and gain_bit[i] < bmin : corrected.append( self.sine[i] - pos10*scale )
            elif sine_grad[i] < 0. and gain_bit[i-1] < bmin and gain_bit[i] > bmax : corrected.append( self.sine[i] - neg01*scale )
            elif sine_grad[i] < 0. and gain_bit[i-1] > bmax and gain_bit[i] < bmin : corrected.append( self.sine[i] - neg10*scale )
            else: corrected.append( self.sine[i] )

        self.sine_spikecorr = AutogainSineWave(self.gain, self.freq, self.amp, self.sample, self.index, 
                                               self.adc_counts, self.gain_bit, self.debug, corrected)

        self.sine_spikecorr.spike_values = [ pos01_, pos10_, neg01_, neg10_ ]
        self.sine_spikecorr.spike_method = method
        self.sine_spikecorr.spike_c01 = c01
        self.sine_spikecorr.spike_c10 = c10

        return self.sine_spikecorr

    # -------------------------------------------------------------------------------
    def CorrectAG_Distortion( self ): 
        """Corrects for distortion. The corrections are derived from distorion/residual fits to 1x and 4x data.
        Corrections from 1x (4x) are applied to 1x (4x) sections of the AG wave.

        NOTE: In early datasets (Dec 2018), distortion (especially 3rd harmonic, but also 5th harmonic) was a prominent
        feature in the data. Later datasets (Feb 2019) did not have the same high degree of distorion, so correcting
        for it no longer became necessary (in fact, often it could not be "fit" to -- dominated by noise).
        """

        #if self.debug: 
        #    print 'Function: AutogainSineWave.CorrectAG_Distortion()'

        if None in [self.prevfitR_params_1x, self.prevfitR_params_4x]:
            #print "ERROR SineWave.CorrectAG(): fit parameters for residuals of 1x and 4x samples not properly loaded."
            #print "      --> Set these using AutogainSineWave.SetAutogainSineParams()"
            return

        N = self.N
        x = np.arange( N )

        # Scale 1x
        scale = .5

        # Third Harmonic Fits #
        res_1x  = sf.mySine(x, *self.prevfitR_params_1x)
        res_4x  = sf.mySine(x, *self.prevfitR_params_4x)
        res_tot = [ res_1x[i]*self.gain_bit[i] + res_4x[i]*(1-self.gain_bit[i]) for i in range(N) ]

        # Corrected #
        corrected = [ self.sine[i] - res_tot[i] for i in range(N) ]

        self.sine_distcorr = AutogainSineWave(self.gain, self.freq, self.amp, self.sample, self.index, 
                                              self.adc_counts, self.gain_bit, self.debug, corrected)

        return self.sine_distcorr

    # -------------------------------------------------------------------------------
    def CorrectAG_Phase( self ): 
        """Phase correction to account for the phase difference between the 1x and 4x portions of the AG sine wave. In
        1x and 4x data, we observe a substantial phase difference (order of ~ .1 degreee typically, up to ~ 1 degree).
        However, when this phase difference correction is applied to AG data, we do not always see significant
        improvements, suggesting the phase difference in 1x and 4x may be external to the DRE system... Therefore,
        this correction was not used for the Feb 2019 datasets."""

        #if self.debug: 
        #    print 'Function: AutogainSineWave.CorrectAG_Phase()'

        #if None in [self.prevfitR_params_1x, self.prevfitR_params_4x]:
        #    print "ERROR SineWave.CorrectAG(): fit parameters for residuals of 1x and 4x samples not properly loaded."
        #    print "      --> Set these using AutogainSineWave.SetAutogainSineParams()"
        
        if self.fitted_sine is None:
            self.FitSine()

        N = self.N
        x = np.arange( N )
        gain_bit = self.gain_bit

        # Get Fits - for 1x and 4x fitted sines, use the 1x and 4x phase
        fitted_sine    = self.fitted_sine
        fitted_sine_1x = sf.mySine(x, self.fit_params[0], self.fit_params[1], self.prevfit_params_1x[2], self.fit_params[3])
        fitted_sine_4x = sf.mySine(x, self.fit_params[0], self.fit_params[1], self.prevfit_params_4x[2], self.fit_params[3])

        # Get phase correction
        corr_1x  = [ (fitted_sine_1x[i] - fitted_sine[i]) for i in range(N) ]
        corr_4x  = [ (fitted_sine_4x[i] - fitted_sine[i]) for i in range(N) ]
        corr_tot = [ corr_1x[i]*gain_bit[i] + corr_4x[i]*(1.-gain_bit[i]) for i in range(N) ]

        # Scale -- optional, default = 1 (no scaling)
        scale = 1.0 
        corr_tot = [ elt*scale for elt in corr_tot ]
        corr_tot_amp = (max(corr_tot) - min(corr_tot))*.5

        corrected = [ self.sine[i] - corr_tot[i] for i in range(N) ]

        self.sine_phasecorr = AutogainSineWave(self.gain, self.freq, self.amp, self.sample, self.index, 
                                               self.adc_counts, self.gain_bit, self.debug, corrected)

        return self.sine_phasecorr

    #
    # -------------------------------------------------------------------------------
    # Reset #
    # -------------------------------------------------------------------------------
    #

    # -------------------------------------------------------------------------------
    def ResetAll(self):
        """Resets parameters to initialized values"""

        # Params defines in SineWave Class ------------------
        self.Reset()

        # Autogain Calibration ------------------------------
        self.gain_calc = None
        self.offset_1x = None
        self.offset_4x = None
        self.prevfit_params_1x = None
        self.prevfit_params_4x = None
        self.prevfitR_params_1x = None
        self.prevfitR_params_4x = None

        self.sine_distcorr  = None
        self.sine_spikecorr = None
        self.sine_phasecorr = None

        self.spike_values = None
        self.spike_method = None
        self.spike_c01 = None
        self.spike_c10 = None






