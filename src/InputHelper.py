###################################################################################################
#
#  This file provides supporting functions for reading in and processing input arguments. 
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#  Updated: May 2019
#  
####################################################################################################

import argparse
from argparse import RawTextHelpFormatter
import sys
import matplotlib.pyplot as plt
import h5py
import os.path
import numpy as np

# --------------------------------------------------------------------------------------------------
def examples():
    
    ex  = '\n\n\nExamples:'
    # 1
    ex += '\n  python PlotMaster.py -p FFT -g 1x -f 1 -a 0p88 -i 6  '
    ex += '\n    -- Plots the FFT of the file with f=1MHz, a=0.88 V, gain=1x, index=6'
    # 2
    ex += '\n  python PlotMaster.py -p Fit -g 1x -f 1 -a 0p88 -c amp'
    ex += '\n    -- Plots the fitted sine of all files with f=1 MHz, gain=1x, '
    ex += 'index=default (it compares amplitudes)'
    # 3
    ex += '\n  python PlotMaster.py -p Residual -f 1 -a 0p88 -i 0 '
    ex += '\n    -- Runs the script with f=1MHz, a=0.88 V, gain=AG, over files with '
    ex += 'previously averaged sines (i=0 makes you run on the averages)'
    # 4
    ex += '\n  python CalibrateAG.py -g AG -f 1 -a 0p88 -c freq'
    ex += '\n    -- Runs the script with a=0.88 V, gain=AG over all frequencies'
    return ex

# --------------------------------------------------------------------------------------------------
def myFloat( input_string ):
    """String to float, 'p' to '.'"""
    if input_string is None:
        return None
    return float( input_string.replace('p', '.') )

# --------------------------------------------------------------------------------------------------
def myStringMult( input_string , scale ):
    """To muliply strings (ie, amp inputs 1x/4x)"""
    output_float  = myFloat( input_string )*scale
    output_string = str( output_float )
    return output_string.replace('.', 'p')

# --------------------------------------------------------------------------------------------------
def isDataGood( gain, freq, amp ):
    """ Put bad data files here """
    if (gain, freq, amp) == ( '4x', '19p5', '0p22'):
        return False
    if (gain, freq, amp) == ('1x', '10p6', '0p4'):
        return False
    if (gain, freq, amp) == ('1x', '13', '0p4'):
        return False

    return True

# --------------------------------------------------------------------------------------------------
def validGains():
    """List of valid gains in dataset"""
    return [ '1x', '4x', 'AG' ]

# --------------------------------------------------------------------------------------------------
def validFreqs(debug=False):
    """List of valid freqs in dataset"""
    #return [ '0p2', '1', '2', '5', '8', '10p6', '13', '19p5' ] # Data Dec 4 2018
    #return [ '0p3', '1',  '5', '8', '10p6', '13', '19p5' ] # Data Feb 6/7 2019

    configfilepath = "../config/ValidFreqs.txt"
    if debug: print("Reading in spike values from",configfilepath)

    return [line.rstrip('\n') for line in open(configfilepath)]

# --------------------------------------------------------------------------------------------------
def validAmps(debug=False):
    """List of valid amps in dataset"""
    #return [ '0p22', '0p5', '0p9' ] # Data Dec 4 2018
    #return [ '0p1', '0p18', '0p22', '0p4', '0p72', '0p88' ] # Data Feb 6/7 2019

    configfilepath = "../config/ValidAmps.txt"
    if debug: print("Reading in spike values from",configfilepath)

    return [line.rstrip('\n') for line in open(configfilepath)]

# --------------------------------------------------------------------------------------------------
def validIdxs(debug=False):
    """List of valid file indices in dataset"""
    #return [ str(i) for i in np.arange(1,30) ] # Data Dec 4 2018
    #return [ str(i) for i in np.arange(1,29) ] # Data Feb 6/7 2019
    configfilepath = "../config/ValidIdxs.txt"
    if debug: print("Reading in spike values from",configfilepath)

    # only store initial and final numbers
    init_final = [int(line.rstrip('\n')) for line in open(configfilepath)]

    return [ str(i) for i in np.arange(init_final[0],init_final[1]+1) ]

# --------------------------------------------------------------------------------------------------
def validSamplingFreqs():
    """List of valid sampling frequencies in dataset"""
    return ['40', '10']

# --------------------------------------------------------------------------------------------------
def validComparisons():
    """List of valid comparison options in dataset"""
    return [ 'gain', 'freq', 'amp', 'index', 'special']

# --------------------------------------------------------------------------------------------------
def checkInputs( parser ):
    """Checks if inputs are ok (if they're in their associated "valid" list)."""

    inputsOK = True

    args = parser.parse_args()	

    # Standard Checks
    if args.gain not in validGains() and args.compare != 'gain':  
        print(' - ERROR: Invalid gain input. Options: [-g]=', validGains())
        inputsOK = False
    if args.freq not in validFreqs() and args.compare != 'freq': 
        print(' - ERROR: Invalid freq input. Options: [-f]=', validFreqs())
        inputsOK = False
    if args.amp not in validAmps() and args.compare != 'amp':
        print(' - ERROR: Invalid amp input. Options: [-a]=', validAmps())
        inputsOK = False
    if args.idx not in validIdxs() and args.idx != '0' and args.compare != 'index':  
        print(' - ERROR: Invalid index input. Options: [-i]=', validIdxs())
        inputsOK = False
    if args.sample not in validSamplingFreqs():  
        print(' - ERROR: Invalid sampling freq input. Options: [-s]=', validSamplingFreqs())
        inputsOK = False
    if args.compare is not None and args.compare not in validComparisons():  
        print(' - ERROR: Invalid comparison input. Options: [-c]=', validComparisons())
        inputsOK = False
    if args.compare == 'special' and args.amp not in validAmps() and args.freq not in validFreqs():
        print( ' - ERROR: If "-c special" option is selected, the amplitude/gain to compare to must be provided.')
        print( ' - Amp options: ', validAmps())
        print( ' - Freq options:', validFreqs())
        inputsOK = False

    # Other Checks
    if args.gain == '4x' and args.compare is None:
        if myFloat(args.amp) > 0.25 : 
            print(' - ERROR: Invalid gain and amplitude combination.')
            inputsOK = False

    if not inputsOK: 
        exit()

    if args.compare is not None:
        print(' - NOTE: option to compare', args.compare,'selected. This might override a single set value')

    return True

# --------------------------------------------------------------------------------------------------
def parseMe( argv=None ):
    """Handles argument parsing"""

    print('Parsing Arguments...')

    parser = argparse.ArgumentParser(epilog=examples(), formatter_class=RawTextHelpFormatter) #usage="Usage")

    # Main params
    parser.add_argument("-g", "--gain" , action="store", dest="gain", default='1x', help="gain")
    parser.add_argument("-f", "--freq" , action="store", dest="freq", default = '5', help="frequency in MHz")
    parser.add_argument("-a", "--amp"  , action="store", dest="amp" , default = '1p0', help="amplitude")
    parser.add_argument("-i", "--index", action="store", dest="idx" , default='1', help="index of file, 2-29. Default = 2")
    parser.add_argument("-s", "--sample" , action="store", dest="sample", default='40', help="sampling frequency. Default = 40MSPS")
    parser.add_argument("-t", "--textfile", action="store", dest="file",default='20200102_cv3tb_sine_4p99_32bitMode_ch8_SARinput.txt', help='name of input file to run over')
    parser.add_argument("-d", "--doCalib", action="store", dest="doCalib",default=False, help='do calibration')

    # Additional params
    parser.add_argument("-p", "--plottype"    , action="store", dest="plottype", help="for PlotMaster.py only: what parameter you want to plot")
    parser.add_argument("-c", "--compare" , action="store", dest="compare",help="parameter to compare (gain, freq, amp)")
    parser.add_argument("-v", "--verbose" , action="store_true", dest="debug", help="verbose output (sets debug=True)")
    parser.add_argument("-o", "--output"  , action="store", dest="output", help="force output file name (in PlotsSaved/)")

    #if not checkInputs(parser):
    #    print 'ERROR: Inputs not ok. Exiting.'
    #    quit()

    args = parser.parse_args()

    # Comparison Handler
    #if args.compare == 'gain'   : args.gain = validGains()
    #if args.compare == 'freq'   : args.freq = validFreqs(args.debug)
    #if args.compare == 'amp'    :
    #    if args.gain == '1x': args.amp  = validAmps(args.debug)
    #    if args.gain == '4x': args.amp  = validAmps(args.debug)[:2]
    #if args.compare == 'index'  : args.idx  = validIdxs(args.debug)
    #if args.compare == 'special': 
    #    if args.gain == '1x' and myFloat( args.amp ) < 0.25:
    #        print ' - ERROR: Invalid gain and amplitude combination for "special" comparison'
    #        exit()
    #    if args.gain == '4x':
    #        args.gain = '1x'
    #        args.amp = myStringMult( args.amp, 4.0 )
    #        print ' - NOTE: Saving special output in terms of 1x inputs'
    #    args.compare = 'special'+args.gain+args.amp
    if args. compare is None: args.compare = ''

    print('Arguments OK')
    return args

# --------------------------------------------------------------------------------------------------
def datapath( gain, freq, amp, sample, idx ):
    """Gets the dataset path"""

    configfilepath = '../config/InputFileType.txt'

    file_type = [line.rstrip('\n') for line in open(configfilepath)][0]

    valid_file_types = ['.npy', '.hdf5']
    #if file_type not in valid_file_types:
        #print 'Error! File Type "',file_type,'" in', configfilepath, 'is not valid! Valid file types are:',valid_file_types

    path = ''
    if file_type == '.npy':
        path = '../data/npy/'+sample+'MSPS/Sine_'+freq+'MHz/Amp'+amp+'_'+gain+'_'+idx+'.npy'
    elif file_type == '.hdf5':
        path = '../data/hdf5/'+sample+'MSPS/Sine_'+freq+'MHz/Amp'+amp+'_'+gain+'_'+idx+'.hdf5'

    #if not isDataGood(gain, freq, amp):
        #print 'WARNING: File in Bad Data List: ', path, ', --> skipping...'
	
    if idx=='0' and file_type == '.npy':
        path = '../data/npy/'+sample+'MSPS/Sine_'+freq+'MHz/Averaged/Amp'+amp+'_'+gain+'_Averaged.npy'
        # Don't yet have a way to read in averaged datapaths from hdf5! 

    if not os.path.isfile(path):
        #print 'ERROR: Cannot open', path
        return
    else:
        return path

# --------------------------------------------------------------------------------------------------
def loadData( datapath, index=-1 ):

    if '.npy' in datapath:
        adc_counts = np.load( datapath )[0]
        gain_bit   = np.load( datapath )[1]
        return [adc_counts, gain_bit]

    if 'hdf5' in datapath:
        # Right now only looking at 1 channel of 1 ADC --> will need to change!! 
        adc_index = '1'
        channel   = '1'
        datafile   = h5py.File(datapath, 'r')
        rootpath   = 'Measurement_'+str(index)+'/coluta'+adc_index+'/channel'+channel
        adc_counts = f[rootpath+'/samples'][()]
        gain_bit   = f[rootpath+'/bits'][()]
        return [adc_counts, gain_bit]
   
    if 'txt' in datapath:
        adc_index = '1'
        channel   = '1'
        gain_bit = 0 
        adc_counts = ReadRawBits(datapath)
        return [adc_counts, gain_bit]

# --------------------------------------------------------------------------------------------------
def loopGrid( gain, freq, amp, idx, compare ):
    """Creates a loop grid"""

    loop_grid = []

    if compare == 'gain' :
        loop_grid = [ (g   , freq, amp, idx) for g in gain ]
    if compare == 'freq' :
        loop_grid = [ (gain, f   , amp, idx) for f in freq ]
    if compare == 'amp'  :
        loop_grid = [ (gain, freq, a  , idx) for a in amp ]
    if compare == 'index':
        loop_grid = [ (gain, freq, amp, i  ) for i in idx  ]
    if 'special' in compare:
        loop_grid = [ ('4x', freq, myStringMult( amp, 0.25 ), idx), ('1x', freq, amp, idx) ]
                
    if compare == '' : loop_grid = [ (gain, freq, amp, idx) ]

    return loop_grid

# --------------------------------------------------------------------------------------------------
def GetSpikeValues():

    configfilepath = "../config/SpikeValues.txt"
    #print "Reading in spike values from",configfilepath

    spikevalues = [line.rstrip('\n') for line in open(configfilepath)]

    return spikevalues

# --------------------------------------------------------------------------------------------------
def ReadRawBits(infile):
  in_file = open(infile,"read")
  sar_weights = [0,0,0,0,0,0,0,0,0,0,0,0,3584, 2048, 1024, 640, 384, 256, 128, 224, 128, 64, 32, 24, 16, 10, 6, 4, 2, 1, 0.5, 0.25]
  sine_arr = np.zeros(8184)
  #sine_arr = np.zeros(4096)
  index = 0

  for line in in_file:
    sample_val = 0.0
    arr_bits = list(line)

    for bit in range(len(arr_bits)-1):
      if bit < 12: continue #mask bits 0-11
      new_val_bit = sar_weights[bit] * int(arr_bits[bit])
      sample_val += new_val_bit

    sine_arr[index] = sample_val
    index = index + 1 

  return sine_arr

# --------------------------------------------------------------------------------------------------
def Read32ModeBits(infile,n_lines):
  in_file = open(infile,"read")
  #Ideal 3584	2048	1024	640	384	256	128	224	128	64	32	24	16	10	6	4	2	1
  sar_weights = [0,0,0,0,0,0,0,0,0,0,0,0,3584, 2048, 1024, 640, 384, 256, 128, 224, 128, 64, 32, 24, 16, 10, 6, 4, 2, 1, 0.5, 0.25]
  #Calibrated 3584	2050.2	1026.4	642.35	386.17	258.16	128.67	222.86	128.09	63.575	32.071	23.822	16.047	9.6687	5.5902	3.8057	1.9214	0.98862
  #sar_weights = [0,0,0,0,0,0,0,0,0,0,0,0,3584 , 2050.2  ,1026.4  ,642.35  ,386.17,  258.16 , 128.67 , 222.86 , 128.09 , 63.575,  32.071 , 23.822 , 16.047 , 9.6687,  5.5902 , 3.8057,  1.9214,  0.98862, 0.5, 0.25]
  #sar_weights = [1]*32
  sine_arr = np.zeros(n_lines)
  index = 0
  arr0 = []
  
  for line in in_file:
    sample_val = 0.0
    arr_bits = list(line)

    for bit in range(len(arr_bits)-1):
      if bit < 12: continue #mask bits 0-11
      new_val_bit = sar_weights[bit] * int(arr_bits[bit])
      sample_val += new_val_bit

    sine_arr[index] = sample_val
    arr0.append(sample_val)
    index = index + 1 
  
  if len(arr0) > 0: x0 = plt.hist(arr0,label='MDAC Scale 0')
  if len(arr0) > 0: print('min 0 ,', min(x0[1]), ', max ' , max(x0[1]))

  plt.xlabel('Samples [ADC Counts]')
  plt.legend(loc=1, frameon=False)
  #plt.show()
  plt.savefig('../results/hist_adccounts_'+infile.split('/')[3].split('.')[0]+'.pdf')
  print('Saved fig: ', '../results/hist_adccounts_'+infile.split('/')[3].split('.')[0]+'.pdf')
  plt.clf()
  plt.cla()
  plt.close()

  return sine_arr
 
# --------------------------------------------------------------------------------------------------
def ReadMDACBits(infile,n_lines, doCalib):
  in_file = open(infile,'r')
   
  #  count_number_lines = count_number_lines + 1
  #print('count: ', count)

  sine_arr = np.zeros(n_lines)
  gain_arr = np.zeros(n_lines)
  index = 0
  gain_index = 11 
  
  #MDACCorrectionCode7: 11100000000000000
  #MDACCorrectionCode6: 11000000000000000
  #MDACCorrectionCode5: 10100000000000000
  #MDACCorrectionCode4: 10000000000000000
  #MDACCorrectionCode3: 01100000000000000
  #MDACCorrectionCode2: 01000000000000000
  #MDACCorrectionCode1: 00100000000000000
  #MDACCorrectionCode0: 00000000000000000
  mdac_codes = [114688, 98304, 81920, 65536, 49152, 32768, 16384, 0] #defaults
  mdac_codes = [0,0,0,0,0,0,0,0] #for calibration

  sar_weights = [0,0,0,0,0,0,0,0,0,0,0,0,3584, 2048, 1024, 640, 384, 256, 128, 224, 128, 64, 32, 24, 16, 10, 6, 4, 2, 1, 0.5, 0.25]

  arr0 = []
  arr1 = []
  arr2 = []
  arr3 = []
  arr4 = []
  arr5 = []
  arr6 = []
  arr7 = []
  arr8 = []
  
  for line in in_file:
    sample_val = 0.0
    arr_bits = list(line)

    #MDAC computation
    for bit in range(4,13): #go one extra bit to see if all 0s
      if(int(arr_bits[bit]) > 0): 
        gain_index = bit - 4
        #print('found, breaking and adding ' , mdac_codes[bit-5])
        #print(gain_index)
        sample_val += np.divide(mdac_codes[bit-5],4)   
        break 

    #SAR computation
    for bit in range(len(arr_bits)-1): #remove sub LSBs
      if bit < 12: continue #mask bits 0-11
      else: new_val_bit = sar_weights[bit] * int(arr_bits[bit])
      sample_val += new_val_bit
   
    #bit index numbering scheme: 
    #arr_bits           4 5 6 7 8 9 10 11  
    #gain index         0 1 2 3 4 5  6 7 8 (all zeros)
    #MDAC bit     (MSB) 7 6 5 4 3 2 1 0 (LSB)
    #correction   114668 98304 81920 65536 49152 32768 16384 0
 
    sine_arr[index] = sample_val
    gain_arr[index] = gain_index
    if gain_arr[index] == 8: arr0.append(sample_val) 
    if gain_arr[index] == 7: arr1.append(sample_val)
    if gain_arr[index] == 6: arr2.append(sample_val)
    if gain_arr[index] == 5: arr3.append(sample_val)
    if gain_arr[index] == 4: arr4.append(sample_val)
    if gain_arr[index] == 3: arr5.append(sample_val)
    if gain_arr[index] == 2: arr6.append(sample_val)
    if gain_arr[index] == 1: arr7.append(sample_val)
    if gain_arr[index] == 0: arr8.append(sample_val)
    index = index + 1 

  if doCalib:
    if len(arr8) > 0: x8 = plt.hist(arr8,label='MDAC Scale 8')
    x7 = plt.hist(arr7,label='MDAC Scale 7')
    x6 = plt.hist(arr6,label='MDAC Scale 6')
    x5 = plt.hist(arr5,label='MDAC Scale 5')
    x4 = plt.hist(arr4,label='MDAC Scale 4')
    x3 = plt.hist(arr3,label='MDAC Scale 3')
    x2 = plt.hist(arr2,label='MDAC Scale 2')
    x1 = plt.hist(arr1,label='MDAC Scale 1')
    if len(arr0) > 0: x0 = plt.hist(arr0,label='MDAC Scale 0')

    if len(arr8) > 0: 
      print('min 8 ,', min(x8[1]), ', max ' , max(x8[1]), ', corr: none')
      print('min 7 ,', min(x7[1]), ', max ' , max(x7[1]), ', corr: ', max(x7[1]) - min(x8[1]))
    print('min 6 ,', min(x6[1]), ', max ' , max(x6[1]), ', corr: ', max(x6[1]) - min(x7[1]))
    print('min 5 ,', min(x5[1]), ', max ' , max(x5[1]), ', corr: ', max(x5[1]) - min(x6[1]))
    print('min 4 ,', min(x4[1]), ', max ' , max(x4[1]), ', corr: ', max(x4[1]) - min(x5[1]))
    print('min 3 ,', min(x3[1]), ', max ' , max(x3[1]), ', corr: ', max(x3[1]) - min(x4[1]))
    print('min 2 ,', min(x2[1]), ', max ' , max(x2[1]), ', corr: ', max(x2[1]) - min(x3[1]))
    print('min 1 ,', min(x1[1]), ', max ' , max(x1[1]), ', corr: ', max(x1[1]) - min(x2[1]))
    if len(arr0) > 0: print('min 0 ,', min(x0[1]), ', max ' , max(x0[1]), ', corr: ', max(x0[1]) - min(x1[1]))
    plt.xlabel('Samples [ADC Counts]')
    plt.legend(loc=1, frameon=False)
    #plt.show()
    plt.savefig('../results/hist_adccounts_'+infile.split('/')[3].split('.')[0]+'.pdf')
    print('Saved fig: ', '../results/hist_adccounts_'+infile.split('/')[3].split('.')[0]+'.pdf')
    plt.clf()
    plt.cla()
    plt.close()

  return [sine_arr, gain_arr]


# --------------------------------------------------------------------------------------------------
def convertBinaryToDecimal(infile):
  in_file = open(infile,"read")
  sine_arr = np.zeros(8184)
  index = 0

  for line in in_file:
    sample_val = int(line.strip('\xef \xbb \xbf'),2)

    sine_arr[index] = sample_val
    index = index + 1 

  return sine_arr

