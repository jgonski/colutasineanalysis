#####################################################################################
#
#  Sine-related functions:
#   - mySine (a general sine function)
#   - chi2
#   - maxDiff: returns the maximum difference between 2 arrays
#   - fixPhase: returns a phase in [0, 2pi)
#   - avgPhase: special function to calculate the average phase (necessary because 
#		of periodic boundaries)
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#  Updated: May 2019
#  
#####################################################################################

import math
import numpy as np
from scipy import stats

# -----------------------------------------------------------------------------------
def mySine(x, freq, amp, phase, shift):
    """General Sine Wave function -- used for fitting"""
    return np.sin(x * (2*math.pi*freq) - phase) * amp + shift

# -----------------------------------------------------------------------------------
def chi2( f1, f2 ):
    """Compute chi^2 of 2 functions, same length"""
    
    if len(f1) != len(f2):
        #print 'ERROR: chi2(...) function inputs have different lengths! Returning None...'
        return None
    return sum( pow(abs(f1-f2), 2.0) )

# -----------------------------------------------------------------------------------
def maxDiff( f1, f2 ):
    """Compute the maximum difference of 2 functions, same length"""

    if len(f1) != len(f2):
        #print 'ERROR: maxDiff(...) function inputs have different lengths! Returning None...'
        return None
    
    nskip = 1
    return max( abs(f1[nskip:] - f2[nskip:]) )

# -----------------------------------------------------------------------------------
def fixPhase( n ):
    """Put phase in range [0, 2pi)"""
    n = n % (2.*math.pi)
    if n < 0.0: n += 2.*math.pi
    return n

# -----------------------------------------------------------------------------------
def avgPhase( arr ):
    """Take an array of phases [rad], find average and stdev taking periodic boundary
    conditions into consideration, and put phase in range [0, 2pi)"""

    # If it's just a number
    try:
        if len(arr) == 0: return 0., 0.
    except TypeError:
        return fixPhase(arr), 0.0

    # Deal with phase
    arr = [ fixPhase(a) for a in arr ]
    # make it more granular
    arr_int = [ int(a*5)/5.0 for a in arr ]
    #get one mode
    mode = stats.mode(arr_int)[0][0]
    # center at pi, take mod, then shift back
    arr = [ (( a - mode + math.pi )%( 2*math.pi ) + mode - math.pi )for a in arr ]
    # make sure average is in [0, 2pi)
    avg = fixPhase( np.mean(arr) )
    std  = np.std(arr)

    return avg, std
