# Make the directory structure

# run/
mkdir run

# data/
mkdir -p data/hdf5 data/SineParams # data/npy already exists

# results/
mkdir results
cd results
mkdir CalibrateAG  CheckSineFits  FFTs  Residuals  SineFits  SineParams
mkdir SineParams/Histograms
cd CalibrateAG
mkdir All  ENOBs  FFTs  MainPlots  SpikeValues  StandardDeviations
cd All
mkdir Spike Distortion Phase

cd ../../../
