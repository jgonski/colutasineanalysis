###################################################################################################################
#
#  This script takes raw .csv files from COLUTAv2
#       - First column is decision bit; other columns are adc bits
#       - Rows are samples (in time)
#  Output: A 2D array of two rows and however many colums that there are measured samples from COLUTAv2
#       - First row: Decision bits
#       - Second row: ADC samples
#  Output saved as a binary .npy (numpy) file
#
#  Author: Alan Kahn
#  Modified: Kiley Kennedy <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#
###################################################################################################################

import numpy as np
import csv
import os

# Chip Weight inputs 
chipWeights = [2048.00, 1024.25, 512.50, 256.25, 128.00, 64.00, 130.75, 66.00, 32.75, 16.25, 8.25, 4.00, 2.00, 1.00]
chipWeights = [2046.25, 1023.00, 511.75, 255.75, 128.00, 64.00, 132.75, 67.50, 33.00, 16.75, 8.25, 4.00, 2.00, 1.00]

def getModifiedSample(row):
    modifiedSample = 0
    for i in range(14):
	modifiedSample += float(row[i+1])*chipWeights[i] #Get raw sample 
    return modifiedSample

def main():

    # Input and Output Dirs
    rawDataDir = '/a/home/kolya/rg3048/COLUTAV2_Data/' # Directory of Raw Data from COLUTAv2
    processedDataDir = '../Data/Processed/40MSPS/'     # Output directory for processed data

    # Parameters
    #   NOTE!!! Run numbers and frequencies must correspond to each other; otherwise this will
    #   result in an incorrect naming scheme
    
    runNumbers = [ '422', '423'] #,  '624', '625', '626' ]#, '649', '650'  ]
    freqs      = [ '0p3', '1'  ,  '5'  , '8'  , '10p6', '13' , '19p5' ]
    
    amps       = [ '0p1', '0p18', '0p22', '0p4', '0p72', '0p88' ]
    gains      = ['1x', '4x', 'AG']

    channel = 'CHANNEL1'
    
    runNames = [ 'Run_0'+r+'/' for r in runNumbers ] 
    frequencies = [ 'Sine_'+f+'MHz/' for f in freqs ]

    startSample = 250
    nSamples = 4096

    nFilesPerDataset = 30 # Number of files per frequency/amp/gain combination
    
    for runNum in range(len(runNames)): #Iterate through all runs

        # Get File names in that Run
	filenames = [] 
	for fname in os.listdir(rawDataDir+runNames[runNum]):
            #Naming convention
	    if ".csv" not in fname and channel not in fname: continue
	    filenames.append(fname)
	filenames.sort() 
	
	for iamp in range(len(amps)):
	    for igain in range(len(gains)):
		averagedData = np.zeros([2,nSamples])
			
		#if gains[igain] == "4x": continue
		
		print gains[igain], amps[iamp]				

		for i in range(1, nFilesPerDataset): #Loop through 0.22V files, ***skip the first file***
                    
		    processedData = np.zeros([2, nSamples]) #Initialize a 2D array of 2 rows and nSamples columns. First row will be the array of the 2nd bits in the raw data (decision bits) second row will be the weighted ADC counts

		    #Open the file again and fill the data array
		    with open(str(rawDataDir+runNames[runNum]+filenames[nFilesPerDataset*len(gains)*iamp + nFilesPerDataset*igain + i])) as csvfile:
			reader = csv.reader(csvfile)
			next(reader, None) #Skip the header
			ct=0 #counter
			for row in reader:
			    if ct >= startSample and ct < (nSamples + startSample):
				processedData[0][ct - startSample] = getModifiedSample(row[1::]) #Modified ADC samples
				processedData[1][ct - startSample] = row[1]	#Decision bit			
				averagedData[0][ct - startSample] += processedData[0][ct - startSample]/float(nFilesPerDataset-1)
				averagedData[1][ct - startSample] += processedData[1][ct - startSample]/float(nFilesPerDataset-1)
				ct+=1

			# Check if dir exists -- if not, create it
			if not os.path.exists(os.path.dirname(processedDataDir+frequencies[runNum])):
			    try:
				os.makedirs(os.path.dirname(processedDataDir+frequencies[runNum]))
			    except OSError as exc: # Guard against race condition
                                if exc.errno != errno.EEXIST: raise
                                
			np.save(processedDataDir+frequencies[runNum]+"Amp"+amps[iamp]+"_"+gains[igain]+"_"+str(i)+".npy", processedData) #Save the file
			print "Saved File: "+processedDataDir+frequencies[runNum]+"Amp"+amps[iamp]+"_"+gains[igain]+"_"+str(i)+".npy: "+filenames[nFilesPerDataset*len(gains)*iamp + nFilesPerDataset*igain + i].replace("CHANNEL1_", "").replace("_Binary", "")

                # Check if Averaged/ dir exists -- if not, create it
		if not os.path.exists(os.path.dirname(processedDataDir+frequencies[runNum]+"Averaged/")):
		    try:
			os.makedirs(os.path.dirname(processedDataDir+frequencies[runNum]+"Averaged/"))
		    except OSError as exc: # Guard against race condition
			if exc.errno != errno.EEXIST: raise

		np.save(processedDataDir+frequencies[runNum]+"Averaged/Amp"+amps[iamp]+"_"+gains[igain]+"_Averaged.npy", averagedData) #Save the file
		print "Saved File: "+processedDataDir+frequencies[runNum]+"Averaged/Amp"+amps[iamp]+"_"+gains[igain]+"_Averaged.npy"


if __name__ == "__main__":
	main()


