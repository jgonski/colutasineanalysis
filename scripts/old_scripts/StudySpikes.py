###################################################################################################################
#
#  This script specifically studies the spikes in the autogain COLUTAv2 data.
#  It takes no command line arguments -- edit intput/output files in script.
#
#  Author: Kiley Kennedy <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#
###################################################################################################################

import sys
import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import gridspec
from scipy.stats import norm

# Local Modules
import SineFunctions as sf
import CalibrateAG as ag
import InputHelper as ih

#------------------------------------------------------------------------------------------------------------------
def spikeInfoLooper(adc_counts, gain_bit, sine, spikeInfo01, spikeInfo10):
    """ This function collects informatin from a single AG sine wave, in a loop """

    n = len(sine)
    x = np.arange( n )
    pi = math.pi

    # Fit
    fitted_sine, fit_params = sf.fitSine(sine)
    
    freq, amp, phase = fit_params[0], fit_params[1], fit_params[2]
    fitted_sine_deriv = (2*pi*freq)*amp*np.cos(x * (2*pi*freq) - phase)
    
    gain_bit_grad = np.gradient(gain_bit)
    sine_grad = np.gradient(sine)
    adc_counts_grad = np.gradient(adc_counts)
    AGdiff = [ sine[i] - fitted_sine[i] for i in range(n) ]

    spikes = []
                 
    streak = 1
    for i in range(n):
        if i == 0 : continue

        if gain_bit[i] != gain_bit[i-1]:

            # 0->1 Transition
            if gain_bit[i] == 1:
                spikeInfo01['spikeval'].append(AGdiff[i])
                
                spikeInfo01['streak'].append(streak)
                spikeInfo01['sine_i'].append(fitted_sine[i])
                spikeInfo01['sine_im1'].append(fitted_sine[i-1])
                spikeInfo01['sineDeriv_i'].append(fitted_sine_deriv[i])
                spikeInfo01['sineDeriv_im1'].append(fitted_sine_deriv[i-1])
                spikeInfo01['rawADC_i'].append(adc_counts[i])
                spikeInfo01['rawADC_im1'].append(adc_counts[i-1])
                spikeInfo01['rawADCDeriv_i'].append(adc_counts_grad[i])
                spikeInfo01['rawADCDeriv_im1'].append(adc_counts_grad[i])
                spikeInfo01['amp'].append(amp)
                spikeInfo01['freq'].append(freq)
                

            # 1->0 Transition
            if gain_bit[i] == 0:
                spikeInfo10['spikeval'].append(AGdiff[i])
                
                spikeInfo10['streak'].append(streak)
                spikeInfo10['sine_i'].append(fitted_sine[i])
                spikeInfo10['sine_im1'].append(fitted_sine[i-1])
                spikeInfo10['sineDeriv_i'].append(fitted_sine_deriv[i])
                spikeInfo10['sineDeriv_im1'].append(fitted_sine_deriv[i-1])
                spikeInfo10['rawADC_i'].append(adc_counts[i])
                spikeInfo10['rawADC_im1'].append(adc_counts[i-1])
                spikeInfo10['rawADCDeriv_i'].append(adc_counts_grad[i])
                spikeInfo10['rawADCDeriv_im1'].append(adc_counts_grad[i-1])
                spikeInfo10['amp'].append(amp)
                spikeInfo10['freq'].append(freq)
                
                
        # Streak
        if gain_bit[i] != gain_bit[i-1]:
            streak += 1
        else:
            streak = 1

    return spikeInfo01, spikeInfo10

#------------------------------------------------------------------------------------------------------------------
def getSpikeInfo():

    freqs = ip.validFreqs()[:-1] #exclude 19.5 MHz
    amps = ['0p18', '0p22', '0p4', '0p72', '0p88']
    iamps = {'0p1': 0, '0p18': 1, '0p22': 2, '0p4': 3, '0p72': 4, '0p88': 5 }
    idxs = ip.validIdxs()

    n_read = 4096

    gain = 'AG'
    sample = '40'

    # Get stuff for AG
    datapath = '../Data/Intermediate/SinePerformanceParams_02_14_19.npy'
    sinedata  = np.load( datapath ).item()
        
    loop_grid = [(f, a) for f in freqs for a in amps]

    spikeInfo01 = {}
    spikeInfo01['spikeval'] = []
    spikeInfo01['streak'] = []
    spikeInfo01['sine_i'] = []
    spikeInfo01['sine_im1'] = []
    spikeInfo01['sineDeriv_i'] = []
    spikeInfo01['sineDeriv_im1'] = []
    spikeInfo01['rawADC_i'] = []
    spikeInfo01['rawADC_im1'] = []
    spikeInfo01['rawADCDeriv_i'] = []
    spikeInfo01['rawADCDeriv_im1'] = []
    
    spikeInfo01['amp'] = []
    spikeInfo01['freq'] = []

    spikeInfo10 = {}
    spikeInfo10['spikeval'] = []
    spikeInfo10['streak'] = []
    spikeInfo10['sine_i'] = []
    spikeInfo10['sine_im1'] = []
    spikeInfo10['sineDeriv_i'] = []
    spikeInfo10['sineDeriv_im1'] = []
    spikeInfo10['rawADC_i'] = []
    spikeInfo10['rawADC_im1'] = []
    spikeInfo10['rawADCDeriv_i'] = []
    spikeInfo10['rawADCDeriv_im1'] = []
    
    spikeInfo10['amp'] = []
    spikeInfo10['freq'] = []

    # Get average standard deviations... 
    stdevs = [] 
    
    for (freq, amp) in loop_grid:

        if (freq, amp) in [('10p6', '0p4'), ('13', '0p4')]:
            continue

        #print 'Looping on', freq, amp
        
        scale_ = 1.  
        amp_   = amp
        if ih.myFloat( amp ) > 0.25:
            amp_   = '0p22'
            scale_ = ih.myFloat(amp)/0.22
        
        amp1x   = sinedata[( freq , amp , '1x' )][ 'amplC' ]
        amp4x   = sinedata[( freq , amp_ , '4x' )][ 'amplC' ]*scale_
        shift1x = sinedata[( freq , amp  , '1x' )][ 'shftC' ]
        shift4x = sinedata[( freq , amp_ , '4x' )][ 'shftC' ]
        gain_calc = amp4x/amp1x

        sine_corr_ = np.empty(( len(idxs), n_read ))
        gain_bit_  = np.empty(( len(idxs), n_read ))

        for i in idxs:
            j = int(i) - 1
                        
            # Read in data
            datapath = ''
            try:
                datapath = ip.datapath(freq, amp, gain, sample, i)
            except:
		print "Data on bad data list or does not exist; skipping"
		continue

	    print 'Reading in ' + datapath
            data         = np.load( datapath )
            adc_counts   = data[0][:n_read]
            gain_bit     = data[1][:n_read]

            sine_corr = ag.correctAG_raw(adc_counts, gain_bit, gain_calc, shift1x, shift4x)

            sine_corr_[j] = sine_corr
            gain_bit_[j] = gain_bit

            n = len(sine_corr)
            x = np.arange(n)

            # Re-appends...
            spikeInfo01, spikeInfo10 = spikeInfoLooper(adc_counts, gain_bit, sine_corr, spikeInfo01, spikeInfo10)

        n = len(sine_corr_[0])
        sine_corr_avg = [  np.mean( np.transpose( sine_corr_ )[i] ) for i in range(n) ]
        sine_corr_std = [  np.std( np.transpose( sine_corr_ )[i] ) for i in range(n) ]

        stdevs.append(np.mean(sine_corr_std))
        if np.mean(sine_corr_std) > 2:
            print freq, amp, np.mean(sine_corr_std)
    
    plt.hist(stdevs, bins=8, normed=True, alpha=0.6, color='b') #, label='0->1, Avg='+str(round(spikeAvg01,2)))
    plt.xlabel('Standard deviation of a single dataset (freq, amp)')
    plt.ylabel('Number of datasets')
    plt.title('Distribution of standard deviations, 4x')
    plt.show()

    plotDistribution(spikeInfo01, spikeInfo10)

    plotCorrelationMatrix(spikeInfo01, spikeInfo10)

    saveCorrelations(spikeInfo01, spikeInfo10)

    return
    
#------------------------------------------------------------------------------------------------------------------
def plotDistribution(spikeInfo01, spikeInfo10):

    #n01 = len(spikeInfo01)
    #n10 = len(spikeInfo10)
    #x01 = np.arange(n01)
    #x10 = np.arange(n10)

    spikeAvg01 = np.mean(spikeInfo01['spikeval'])
    spikeAvg10 = np.mean(spikeInfo01['spikeval'])
    
    print 'SpikeVal01 =', spikeAvg01
    print 'SpikeVal10 =', spikeAvg10

    # Correct Spike Values (linearly)
    m01, b01 = 7.26366088e-04, -5.13867307e+00
    m10, b10 = 0.00069041, 0.39160267
    spikeCorr01 = spikeInfo01['spikeval']#[ val - ( m01*val + b01 ) for val in spikeInfo01['spikeval'] ]
    spikeCorr10 = spikeInfo10['spikeval']#[ val - ( m10*val + b10 ) for val in spikeInfo10['spikeval'] ]

    #Sandard Deviation Histogram with Fit
    mu01, std01 = norm.fit(spikeCorr01)
    mu10, std10 = norm.fit(spikeCorr10)
    
    y01, x01, _01 = plt.hist(spikeCorr01, bins=40, normed=True, alpha=0.6, color='b', label='0->1, Avg='+str(round(spikeAvg01,2)))
    y10, x10, _10 = plt.hist(spikeCorr10, bins=40, normed=True, alpha=0.6, color='r', label='1->0, Avg='+str(round(spikeAvg10, 2)))

    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, xmax, 100)
    p01 = norm.pdf(x, mu01, std01)#*max(y01)#*y01.sum()
    p10 = norm.pdf(x, mu10, std10)#*max(y10)#*y10.sum()
    plt.plot(x, p01, 'b-', linewidth=2, label="0->1, Fit results: sigma = %.2f, mu = %.2f"% (std01, mu01))
    plt.plot(x, p10, 'r-', linewidth=2, label="1->0, Fit results: sigma = %.2f, mu = %.2f"% (std10, mu10))
    
    #title = "Fit results: sample %d,  sigma = %.2f, mu = %.2f" % (index, mu, std)
    #title = "Fit results: sigma = %.2f, mu = %.2f" % (std, mu)
    
    #plt.hist(spikeInfo10['spikeval'], bins=40, normed=False, alpha=0.6, color='r', label='1->0, Avg='+str(spikeAvg10))
    plt.title('Spike Value Distribution'), plt.xlabel('Spike Value'), plt.ylabel('Number of Spikes [Normalized]')
    plt.legend(loc='upper left', fancybox=True, fontsize='10')
    plt.show()


#------------------------------------------------------------------------------------------------------------------
def plotCorrelationMatrix(spikeInfo01, spikeInfo10):
    
    # for plotting stuff
    data01 = pd.DataFrame(data=spikeInfo01)
    data10 = pd.DataFrame(data=spikeInfo10)

    corr01 = data01.corr()
    corr10 = data10.corr()

    print corr01['spikeval']
    print corr10['spikeval']
        
    i = 0
    for data in [data01, data10]:

        corr = data.corr()
        
        fig = plt.figure(i)
        ax = fig.add_subplot(111)
        cax = ax.matshow(corr,cmap='coolwarm', vmin=-1, vmax=1)
        fig.colorbar(cax)
        ticks = np.arange(0,len(data.columns),1)
        ax.set_xticks(ticks)
        plt.xticks(rotation=90)
        ax.set_yticks(ticks)
        ax.set_xticklabels(data.columns)
        ax.set_yticklabels(data.columns)
        i+=1
        
    plt.show()

#------------------------------------------------------------------------------------------------------------------
def saveCorrelations(spikeInfo01, spikeInfo10):

    corr01 = pd.DataFrame(data=spikeInfo01).corr()
    corr10 = pd.DataFrame(data=spikeInfo10).corr()

    date = '02_28_19'
    spikeInfo01path = '../Data/Intermediate/SpikeInfo01_'+date+'.npy'
    spikeInfo10path = '../Data/Intermediate/SpikeInfo10_'+date+'.npy'
    corr01path = '../Data/Intermediate/SpikeCorrelations01_'+date+'.npy'
    corr10path = '../Data/Intermediate/SpikeCorrelations10_'+date+'.npy'
    
    np.save(spikeInfo01path, spikeInfo01)
    print 'spikeInfo01 saved to', spikeInfo01path
    np.save(spikeInfo10path, spikeInfo10)
    print 'spikeInfo10 saved to', spikeInfo10path
    np.save(corr01path, corr01['spikeval'])
    print 'corr01 saved to', corr01path
    np.save(corr10path, corr10['spikeval'])
    print 'corr10 saved to', corr10path

#------------------------------------------------------------------------------------------------------------------
def plotSavedCorrelations():

    # Get previous spike info
    datapath01  = '../Data/Intermediate/SpikeInfo01_02_19_19.npy'
    spikeInfo01 = np.load( datapath01 ).item()
    datapath10  = '../Data/Intermediate/SpikeInfo10_02_19_19.npy'
    spikeInfo10 = np.load( datapath10 ).item()
    
    i = 0
    for spikeInfo in [spikeInfo01, spikeInfo10]:

        y = spikeInfo['spikeval']
        n = len(y)
        
        #plt.figure(i)
        
        # FIRST CORRECTION (option to add more)
        x1 = spikeInfo['rawADC_im1']
        
        fit1 = np.polyfit(x1,y,1)
        fit_fn1 = np.poly1d(fit1)

        xmin = min(x1)
        xmax = max(x1)
        xstep = (xmax - xmin)/100
        xplot = np.arange(xmin, xmax, xstep)

        print fit1
        
        m = '{:.2E}'.format(fit1[0]) #str(fit1[0]) #round(fit1[0], 4))
        b = '{:.2E}'.format(fit1[1])
        eqn = ' FIT: y = '+m+'*x + '+b
        data = '0->1'
        if i == 1: data = '1->0'
        plt.plot(x1, y, ip.plotStyle(i,'symbols'), label=data+' Data')
        plt.plot(xplot, fit_fn1(xplot), ip.plotStyle(i+2,'lines'), linewidth=2, label=data+eqn)
    
        y = [ y[j] - fit_fn1(x1[j]) for j in range(n) ]

        i+=1

    plt.xlabel('ADC Count [original]'), plt.ylabel('Spike Value [ADC Counts, 14b]')
    plt.legend(loc='best', fancybox=True, fontsize='10')
    plt.show()

#------------------------------------------------------------------------------------------------------------------
def main():

    # These are the two main functions:

    # 1. getSpikeInfo()
    getSpikeInfo()

    # 2. plotSavedCorrelations()
    plotSavedCorrelations()
    
#------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    main()

