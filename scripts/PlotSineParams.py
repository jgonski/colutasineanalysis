###################################################################################################################
#
#  This script plots various sine performance and fit parameters that were extracted in ExtractSineInfo.py
#   - Input file is output file of ExtractSineInfo.py
#   - Input file is read in as the first argument of the script, ie:
#           python PlotSineParams.py path/to/file.npy
#   - See the parameters you can adjust under "Options."
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#  Updated: May 2019
#
###################################################################################################################

import numpy as np
import matplotlib.pyplot as plt
import math

# Local Modules
import sys
sys.path.append("../src/")
import SineFunctions as sf
import InputHelper as ih
import PlotHelper as ph

# OPTIONS ------------------------
sample    = '40'     # sampling frequency
special   = False    # Plot only 1x and 4x with "adc count counterparts" (ie, 1x amp = 0.88, 4x amp = 0.22)
plotAll   = True     # Plot all 1x, 4x
plotHists = False    # If True, only plots histos, nothing else
debug     = False

# PARAMETERS TO PLOT (FROM DICT) ----------------------------------------------------------------------------------
Params = {
        # Stored Values
        'freq'  : {'short': 'Freq'   , 'long': 'Frequency', 'units':'[MHz]'},
        'amp'   : {'short': 'Amp'    , 'long': 'Amplitude', 'units':'[ADC Counts]'},
        'phase' : {'short': 'Phase'  , 'long': 'Phase'    , 'units':'[rad]'},
        'offset': {'short': 'Offset' , 'long': 'Offset'   , 'units':'[ADC Counts]'},
        'chi2'  : {'short': 'Chi^2' , 'long': 'Chi^2'    , 'units':'[ADC Counts]'},                      
        'enob'  : {'short': 'ENOB'  , 'long': 'ENOB'     , 'units':'[bits]' },
        'sfdr'  : {'short': 'SFDR'  , 'long': 'SFDR'     , 'units':'[dB]'},
        'snr'   : {'short': 'SNR'   , 'long': 'SNR'      , 'units':'[dB]'},
        'freqR'  : {'short': 'Freq Res'  , 'long': 'Freq Distortion' , 'units':'[MHz]'},
	'ampR'   : {'short': 'Ampl Res'  , 'long': 'Amp Distortion'  , 'units':'[ADC Counts]'},
        'phaseR' : {'short': 'Phase Res' , 'long': 'Phase Distortion', 'units':'[rad]'},
        'offsetR': {'short': 'Offset Res', 'long': 'Shift Distortion', 'units':'[ADC Counts]'},
        'freqC'  : {'short': 'Freq Corr'  , 'long': 'Corrected Frequency' , 'units':'[MHz]'},
	'ampC'   : {'short': 'Amp Corr'   , 'long': 'Corrected Amplitude' , 'units':'[ADC Counts]'},
        'phaseC' : {'short': 'Phase Corr' , 'long': 'Corrected Phase'     , 'units':'[rad]'},
        'offsetC': {'short': 'Offset Corr', 'long': 'Corrected Shift'     , 'units':'[ADC Counts]'},
        'enobC'  : {'short': 'ENOB Corr'  , 'long': 'Corrected ENOB'      , 'units':'[bits]'},

        # Stored Values - Pre-Calculated        
        'amplRatDist': {'short': 'Amp Rat' , 'long': 'FFT Amplitude Ratio dist/fund' , 'units':''},
        'amplRHand': {'short': 'Res Amp', 'long': 'Residual Amplitude (by hand)', 'units':'[bits]'},

        # Values Calculated HERE! 
        'phaseDiff4x1x':    {'short': 'Phase Diff' , 'long': 'Phase Difference 4x-1x'   , 'units':'[deg]'},
        'phaseDiffFundRes': {'short': 'Phase Diff' , 'long': 'Phase Difference Fund-Res', 'units':'[rad]'},
        'gain':            {'short': 'Gain'       , 'long': 'Calculated Gain'          , 'units':''},         
        'ampDeriv':        {'short': 'Amp Deriv'  , 'long': 'Amplitude Derivative'     , 'units':'[Counts/MHz]'},
        'ampDerivP':       {'short': 'Amp Deriv/IP Amp', 'long': 'Amplitude Derivative/Input Amp' , 'units':'[Counts/(VMHz)]'},
        'enobImp':         {'short': 'ENOB Imp.'       , 'long': 'ENOB Improvement with Dist Corr', 'units':'[bits]'}
        }

#------------------------------------------------------------------------------------------------------------------
def makePlots(param, amp, special = False):
    """This function makes the plots"""
        
    path   = '../results/SineParams/sine_'+param+'_amp'+amp+'.png'
    title  = 'Measured '+Params[param]['long'] + ' vs Input Frequency'
    legend = 'Input Amp='+amp+' V\nGain Type'
    ax1 = 'Input Frequency [MHz]'
    ay1 = Params[param]['short'] + ' ' + Params[param]['units']
    
    #ax2 = 'Input Frequency [MHz]'
    #ay2 = Params[param]['short'] + ' Stdev ' + Params[param]['units']

    if special:
        path   = '../results/SineParams/sine_'+param+'_special.png'
        legend = 'Input'

    if plotAll:
        path   = '../results/SineParams/sine_'+param+'_all.png'
        legend = 'Input'

    if param == 'gain':
        legend = 'Inputs'
        
    plt.title(title)
    plt.xlabel(ax1), plt.ylabel(ay1)
    plt.legend(loc='best', title=legend, fancybox=True, fontsize='10')
    plt.grid(axis='both')

    #plt.show()
    plt.savefig(path)
    print 'Plot saved to', path
    plt.clf()

def main():	

    # Open Input
    datapath = sys.argv[1]
    sinedata = np.load( datapath ).item() # read in as a dictionary

    ### Plot everything vs. frequency ###


    # -------------------------------------------------------------------------------------------------------------
    for param in Params:
        print 'PARAM =', param
        i = 0 # Reset
        for amp in ih.validAmps(): 
            
            if not special and not plotAll: i = 0 # Reset 
            for gain in ih.validGains(): 
                                
                # Always Exclude
                if (gain == '4x' and ih.myFloat(amp) > 0.25) or gain == 'AG': continue
                                
                # Special Exclude: if 1x doesn't have a "partner" 4x to compare to 
                if special is True:
                    if (gain == '1x' and ih.myFloat(amp) < 0.25): continue
                                        
                if debug: print '-----Looping over', param,gain,amp,'-----'
                                
                # Initialize to plot
                x, y, e = [], [], []
                
                # -------------------------------------------------------------------------------------------------
                for freq in ih.validFreqs(): 

                    # Exclude bad data
                    if not ih.isDataGood(gain, freq, amp) :
                        continue

                    try:
                        data = sinedata[(gain, freq, amp)][ param ]                                                
                    except KeyError:
                                                
                        if param not in Params:
                            print 'WARNING: KeyError: '
                            print '  param =', param
                            print ' ', gain, freq, amp 
                            continue
                        data = -1
                                                                                                                                        
                    avg = np.mean(data)
                    std = np.std(data)                        

                    # Deal with frequency
                    if 'freq' in param:
                        avg *= float(sample)
                        std *= float(sample)

                    # Deal with phase
                    if 'phase' in param:
                        avg, std = sf.avgPhase( data )

                    # ---------------------------------------------------------------------------------------------
                    # Calculated Parameters #
                    
                    if param == 'phaseDiff4x1x':
                        if gain != '4x': continue
                        data1x = sinedata[('1x', freq, amp)][ 'phaseC' ]
                        data4x = sinedata[('4x', freq, amp)][ 'phaseC' ]

                        # Phase is a pain, this way works. 
                        avg1x, std1x = sf.avgPhase( data1x )
                        avg4x, std4x = sf.avgPhase( data4x )
                        avg = sf.fixPhase( avg4x - avg1x )
                        std = std1x + std4x

                        # In degrees
                        avg *= 360./(2.0*math.pi) 
                        std *= 360./(2.0*math.pi) 

                        if avg > 180.:
                            avg -= 360.

                    if param == 'phaseDiffFundRes': 
                        dataFund = sinedata[( gain, freq, amp )][ 'phaseC' ]
                        dataRes  = sinedata[( gain, freq, amp )][ 'phaseR' ]

                        # Phase is a pain, this way works. 
                        avgF, stdF = sf.avgPhase( dataFund )
                        avgR, stdR = sf.avgPhase( dataRes )
                        avg = sf.fixPhase( avgF - avgR )
                        std = stdF + stdR

                    if param == 'gain': # ----------------------------------------- #
                        if gain != '4x': continue
                        
                        data1x = sinedata[('1x', freq, amp)][ 'ampC' ]
                        data4x = sinedata[('4x', freq, amp)][ 'ampC' ]

                        avg = np.mean(data4x)/np.mean(data1x)
                        std = avg*( np.std(data1x)/np.mean(data4x) + np.std(data4x)/np.mean(data1x) )

                    if param in ['ampDeriv', 'ampDerivP']: # ------------------------------------- #
                        # Note: Processing Later
                        data = sinedata[( gain, freq, amp)][ 'ampC' ]
                        avg  = np.mean(data)
                        std  = 0.0 # Undefined so far

                    if param == 'enobImp': # -------------------------------------- #
                        preC  = sinedata[( gain, freq, amp )][ 'enob' ]
                        postC = sinedata[( gain, freq, amp )][ 'enobC' ]
                        avg = np.mean(postC) - np.mean(preC)
                        std = np.std(preC) + np.std(postC)
                                                                                                        
                    # Plot Histos ---------------------------------------------------------------------------------
                    if plotHists:
                        if param in ['phaseDiff4x1x', 'phaseDiffFundRes', 'gain', 'ampDeriv', 'ampDerivP', 'enobImp']:
                                print 'Param' , param , 'incompatible with plotHistos option, continuing'
                                continue
                        plotpath_hist = '../results/SineParams/Histograms/hist_'+param+'_freq'+freq+'_amp'+amp+'_'+gain+'.png'
                        plt.hist( data , bins=5, normed=False, color='b') #data - avg
                        plt.xlabel(Params[param]['long']+' '+Params[param]['units'])
                        plt.ylabel('Number of Samples')
                        plt.savefig( plotpath_hist )
                        print 'Histogram saved to', plotpath_hist
                        plt.clf()
                        continue
                                                                               
                    x.append( ih.myFloat( freq ) )
                    y.append( avg  )
                    e.append( std )

                    # End Loop Over Freqs #

                if plotHists: continue

                if param == 'ampDeriv':
                    y = np.gradient( y )

                if param == 'ampDerivP':
                    y = np.gradient( y )/(ih.myFloat(amp)*float(gain.replace('x','')))

                if gain != '4x' and param in ['gain', 'phaseDiff4x1x']:
                    continue

                legend = gain
                if special or plotAll:
                    legend += ' '+amp+' '

                if param == 'gain':
                    legend = amp

                # Plot --------------------------------------------------------------------------------------------
                
                #plt.plot(x, y, ph.plotStyle(i), label=legend)
                plt.errorbar(x, y, yerr=e, color=ph.plotStyle(i, linestyle='colors'), label=legend)

                i+=1
                
                # End Loop Over Gains #

            if plotHists: continue
            if special or plotAll: continue

            makePlots(param, amp)

            # End Loop Over Amps #
                        
        if plotHists: continue
        if special or plotAll:
            makePlots(param, amp, special)

        # End Loop Over Params #

    return # Done
                        	
if __name__ == "__main__":
	main()
