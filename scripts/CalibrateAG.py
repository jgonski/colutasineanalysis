###################################################################################################################
#
#  A script to study and calibrate the autogain sine waves from the COLUTA v2 chip. This script has many
#  functionalities, including several types of corrections you might apply (raw, spikes, distortion, phase).
#  Under "Global Options," you can see how you can adjust various inputs, spike correction methods, and plots.
#  See more descriptions below.
#
#  Command Line Arguments : None
#  Output : Plots (see hard-coded options)
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#  Updated: May 2019
#
###################################################################################################################

import sys
import numpy as np
import math
from scipy.fftpack import fft
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import gridspec

# Local Modules
sys.path.append("../src")
import InputHelper as ih
import PlotHelper as ph
import PlottingFunctions as pf
import SineFunctions as sf
from SineWave import SineWave, AutogainSineWave

#==================================================================================================================
# GLOBAL OPTIONS
#==================================================================================================================

# USE SINE AVERAGES FOR CALIBRATION ? --------------------------------------------- #
""" You always read in individual files because sometimes gain bit flip can vary
between files (despite triggering). So here, you always read the files in and
provide the baseline calibration (scaling and shifting so you get a smooth sine wave).
From there, you can either:
    - Apply the following corrections: useAveragesForCalib = False
    - OR Average *those* BL calibrated sine waves, and apply the following
      corrections to the averaged sine waves: useAveragesForCalib = True
"""
useAveragesForCalib = False
avg_str = ''
if useAveragesForCalib: avg_str = '_avg'

# Spike Method -------------------------------------------------------------------- #
"""
Options (see fuller description where function is defined in SineWaves.py):
    - 'S' = Standard Correction
    - 'P' = Percentage Correction
    - 'M' = Midpoint Correction
    - 'D' = Dynamic Correction
    - 'C' = Constant Correction
"""
SPIKE_METHOD = 'ZD'            # NOTE: Currently configured with many types of spike corrections

# SPIKE VALUES -------------------------------------------------------------------- #
"""Saved spike values in: config/SpikeValues.txt"""

try:
    SpikeValuesConfig = ih.GetSpikeValues()
except IOError:
    SpikeValuesConfig = [3.60, 3.60] # default
    print "WARNING: Could not read in spike values from config file. Using Defaults:",SpikeValuesConfig

SpikeValues = {}
SpikeValues['0.01'] = [round(float(i), 2)   for i in SpikeValuesConfig] # 0.01 bit precision
SpikeValues['0.5']  = [round(float(i)*2)/2. for i in SpikeValuesConfig] # 0.5 bit precision
SpikeValues['0.1']  = [round(float(i), 1)   for i in SpikeValuesConfig] # 0.1 bit precision
SpikeValues['1']    = [round(float(i))      for i in SpikeValuesConfig] # 1 bit precision

SpikeStrs = {}
for precision in SpikeValues: 
    SpikeStrs[precision] = str(SpikeValues[precision][0])+'_'+str(SpikeValues[precision][1])

# SINEDATA PATH ------------------------------------------------------------------- #
sinedata_path = '../data/SineParams/SineParams_latest.npy'

# PLOTTING OPTIONS :) ------------------------------------------------------------- #
"""Choose to enable/disable various plots produced."""

# In functions to study effects of corrections 
PLOT_DIST   = True #False
PLOT_SPIKE  = True #False
PLOT_PHASE  = True #False

# General, to False #see what's going on 
PLOT_FFT    = True #False
PLOT_STDEV  = True #False # only works with 'useAveragesForCalib = True' option

# Summary/integrative plots (Only Saved, Not Show)
PLOT_MAIN   = True #False  # For each frequency/amplitude combination, generally
PLOTS_FINAL = True #False  # ENOB and Spike Value plots

#==================================================================================================================
# FUNCTIONS
#==================================================================================================================
#
#  Functions in thkis script
#   - CalibrateAG(gain, freq, amp, sample, compare, debug)
#   - ApplyCorrections( SineWave_AG, SineWave_AGx, enobs ) --> SineWave_AG and _AGx are SineWave objects
#   - ConstructAGx(data1x, data4x, gain_bit)
#   - MultiPlot( SineWave, CorrectedSineWave, plot_type, plotpath_ext='')
#
#------------------------------------------------------------------------------------------------------------------

#==================================================================================================================
def main():
    """ The main function. Handles arguments, (some) looping, and ENOB and spike value summary plots."""

    # Parse
    args = ih.parseMe( sys.argv )
    gain, freq, amp, idx, sample, compare = args.gain, args.freq, args.amp, args.idx, args.sample, args.compare
    debug = args.debug

    # -- Only for one dataset ------------------------------------------------------------------------------------- 
    if compare is '':
        print 'Data:', gain, freq, amp
        
        # ----- RUN CALIB ------------------------------------------- #
        output = CalibrateAG(gain, freq, amp, sample, compare, debug) 
        # ----------------------------------------------------------- #
       
        return

    # -- Multiple datasets ---------------------------------------------------------------------------------------- 

    # Read in here to compare ENOB plots to 1x
    sinedata  = np.load( sinedata_path ).item() 
        
    if compare == 'freq': freq = ih.validFreqs()[:-1] #exclude 19.5 MHz
    if compare == 'amp' : amp = ih.validAmps()
    
    if sample == '10':
        freq = ['0p2', '1', '2']
        amp  = ['0p22', '0p9'] #really 0p88

    # Initialize things to plot
    #   NOTE: this enob_vs dict is across diff freqs/amps
    #   (the enobs dict is for enobs within a single amp&frequency combo)
    
    # Enobs for plotting
    enobs_ = {}
    enobs_['1x']       = [[], []] 
    enobs_['AG']       = [[], []]
    enobs_['AGx']      = [[], []]
    enobs_['spikeU']   = [[], []] # Unique for each amp&freq combo

    for precision in SpikeStrs: 
        enobs_['spikeC_'+SpikeStrs[precision]] = [[], []]

    # spike values and their stdevs
    pos01vals = []
    pos10vals = []
    neg01vals = []
    neg10vals = []        
    pos01stds = []
    pos10stds = []
    neg01stds = []
    neg10stds = []

    xvals     = []
    loop_grid = []
    if compare == 'freq': loop_grid = [(f   , amp) for f in freq ]
    if compare == 'amp' : loop_grid = [(freq, a  ) for a in amp  ]
    #if compare == 'all' : loop_grid = [(f, a) for f in ih.validFreqs() for a in ih.validAmps()  ]
    
    for (f, a) in loop_grid:

        if not ih.isDataGood(gain, f, a):
            print 'Skipping Data on bad data list:', gain, f, a
            continue
        else:
            print 'READING IN Data:', gain, f, a

        if compare == 'freq': xvals.append( ih.myFloat( f ) )
        if compare == 'amp' : xvals.append( ih.myFloat( a ) )
    
        # Sine Wave Params - only available for averages... 
        enobs_['1x'][0].append( sinedata[( '1x', f, a  )][ 'enobC' ] )
        enobs_['1x'][1].append( 0.0 )

        # ----- RUN CALIB ----------------------------------------------------------- #
        enobs, spikesAvg, spikesStd = CalibrateAG(gain, f, a, sample, compare, debug)
        # --------------------------------------------------------------------------- #

        # Get ENOBs
        for enob_type in enobs_:
            if enob_type == '1x': continue
            enobs_[enob_type][0].append( np.mean(enobs[enob_type]) )
            enobs_[enob_type][1].append( np.std(enobs[enob_type]) )
        
        # Get Spike Values
        [ pos01val, pos10val, neg01val, neg10val ] = spikesAvg
        [ pos01std, pos10std, neg01std, neg10std ] = spikesStd

        pos01vals.append(pos01val)
        pos10vals.append(pos10val)
        neg01vals.append(neg01val)
        neg10vals.append(neg10val)

        pos01stds.append(pos01std)
        pos10stds.append(pos10std)
        neg01stds.append(neg01std)
        neg10stds.append(neg10std)

    if not PLOTS_FINAL:
        return

    #--------------------------------------------------------------------------------------------------------------
    xlabel_, plotpath_ext = '', ''
    compare == 'all'
    if compare == 'freq':
        xlabel_      = 'Input Frequency [MHz]'
        plotpath_ext = 'amp'+amp
        
    if compare == 'amp':
        xlabel_      = 'Input Amplitude [V]'
        plotpath_ext = 'freq'+freq

    #--------------------------------------------------------------------------------------------------------------
    # ENOBS 
    #--------------------------------------------------------------------------------------------------------------

    # Enob Inprovement: Function -------------------------------------------------- #
    def enobImp( enob_arr ):
        n = len(enobs_['spikeU'][0])
        diff = [ enob_arr[i] - enobs_['spikeU'][0][i] for i in range(n) ]
        return diff

    # Plot ENOBS with different Spike Corrs #
    colors = [ 'b', 'g', 'm', 'c' ]

    plt.figure(1)
    plt.clf()
    gs = gridspec.GridSpec(2, 1, height_ratios=[3.5, 2])

    # ----------------------------------------------------------------------------- #
    plt.subplot(gs[0]) 
    plt.title('Autogain ENOBs')
    plt.ylabel('ENOB [14b scale]')
    
    plt.errorbar(xvals, enobs_['AG'][0], yerr=enobs_['AG'][1], color='k', linewidth=2, label='No Corrections')
    plt.errorbar(xvals, enobs_['spikeU'][0], yerr=enobs_['spikeU'][1], color='r', label='Spike "Unique"')

    i = 0
    for precision in SpikeStrs: 
        c01 = SpikeValues[precision][0]
        c10 = SpikeValues[precision][1]                                   
        label_ = '0->1: ' + str(c01) + '; 1->0: ' + str(c10)
        label_ = str(precision)+' LSB'
        plt.errorbar(xvals, enobs_['spikeC_'+SpikeStrs[precision]][0], yerr=enobs_['spikeC_'+SpikeStrs[precision]][1],
                        color=colors[i], label='Spike "Constant": '+label_)
        i+=1

    plt.legend(loc='best', title='Correction Type', fancybox=True, fontsize='8')
    plt.grid(b=True, axis='both')

    # ----------------------------------------------------------------------------- #
    plt.subplot(gs[1])
    plt.ylabel('ENOB Imp wrt SpikeU'), plt.xlabel( xlabel_ )
    
    #plt.plot(xvals, enobImp(enobs_vs['spikeU'][0]), 'r.-')
    #plt.plot(xvals, enobImp(enobs_1x) , 'b.-') #ALWAYS AVERAGED
    
    i = 0
    for precision in SpikeStrs: 
        plt.plot(xvals, enobs_['spikeC_'+SpikeStrs[precision]][0] , colors[i]+'.-')
        i+=1

    plt.grid(b=True, axis='both')

    plotpath_ = '../results/CalibrateAG/ENOBs/AG_ENOB'+avg_str+'_'+plotpath_ext+'_'+sample+'MSPS'+'_spikeCompare.png'
    plt.savefig( plotpath_ )
    print 'Plot saved to', plotpath_
    
    #----------------------------------------------------------------------------------------------------------
    # Spike Values
    #----------------------------------------------------------------------------------------------------------

    plt.figure(2)
    plt.clf()
    plt.title('Autogain Spike Values')
    plt.ylabel('ADC Counts'), plt.xlabel(xlabel_)
    
    plt.errorbar(xvals, pos01vals, yerr=pos01stds, color='r', label='Deriv > 0, 0->1')
    plt.errorbar(xvals, pos10vals, yerr=pos10stds, color='m', label='Deriv > 0, 1->0')
    plt.errorbar(xvals, neg01vals, yerr=pos01stds, color='b', label='Deriv < 0, 0->1')
    plt.errorbar(xvals, neg10vals, yerr=pos10stds, color='g', label='Deriv < 0, 1->0')

    #plt.ylim(-16, 16)
    plt.legend(loc='best', fancybox=True, fontsize='10')
    plt.grid(b=True, axis='both')

    plotpath_ = '../results/CalibrateAG/SpikeValues/AGSpikeValues'+avg_str+'_'+plotpath_ext+'_'+sample+'MSPS_spike'+SPIKE_METHOD+'.png'
    plt.savefig( plotpath_ )
    print 'Plot saved to', plotpath_

#==================================================================================================================
def CalibrateAG( gain, freq, amp, sample, compare, debug):
    """This function applies calibrations for a given gain, freq, amp, sample, etc. Reads in files and gets
    saved sine wave params."""

    useEnobCorr = False

    n_read = 4096
    if sample == '10': n_read = 4000

    idxs = ih.validIdxs()
    idx = '0'
    compare = 'idxs'

    # Read in saved parameters ---------------------------------------------------- #

    # Read in calculated sine wave params
    sinedata = np.load( sinedata_path ).item() 

    # Deal with 4x (params not available at all amplitudes)
    scale_ = 1.  
    amp_   = amp
    if ih.myFloat( amp ) > 0.25:
        amp_ = [a for a in ih.validAmps() if ih.myFloat(a) < 0.25][-1]
        print amp_
        scale_ = ih.myFloat(amp)/ih.myFloat(amp_)

    # Initialize 1x and 4x sine waves; adc_counts and gain bits currently empty
    SineWave_1x_empty = SineWave('1x', freq, amp , sample, '0' , np.zeros(n_read), np.zeros(n_read), debug)
    SineWave_4x_empty = SineWave('4x', freq, amp_, sample, '0' , np.zeros(n_read), np.zeros(n_read), debug)

    # Read in Sine Wave Params
    SineWave_1x_empty.LoadSineData( sinedata_path )
    SineWave_4x_empty.LoadSineData( sinedata_path )

    # Correct 4x Amplitudes
    SineWave_4x_empty.prevfit_params[1]  *= scale_
    SineWave_4x_empty.prevfitC_params[1] *= scale_
    gain_calc = SineWave_4x_empty.prevfit_params[1]/SineWave_1x_empty.prevfit_params[1]
    SineWave_4x_empty.prevfitR_params[1] *= gain_calc
        
    # Sine Wave Params (+ residual)
    fit_params_1x = SineWave_1x_empty.prevfit_params           
    fit_params_4x = SineWave_4x_empty.prevfit_params
    fit_params_res_1x = SineWave_1x_empty.prevfitR_params           
    fit_params_res_4x = SineWave_4x_empty.prevfitR_params
    offset4x = fit_params_4x[3]
    
    # Initialize ------------------------------------------------------------------ #

    gain_bit_         = np.empty(( len(idxs), n_read ))
    adc_counts_       = np.empty(( len(idxs), n_read ))
    sineAG_baseline_  = np.empty(( len(idxs), n_read ))
    sineAGx_baseline_ = np.empty(( len(idxs), n_read ))

    spikeVals_     = [None] * 4
    for k in range(4): spikeVals_[k] = []

    # INIT ENOBS AGAIN
    enobs        = {}
    enobs['1x']       = [] 
    enobs['AG']       = [] # baseline
    enobs['AGx']      = []
    enobs['spikeU']   = [] # Unique for each amp&freq combo
    for precision in SpikeStrs: 
        enobs['spikeC_'+SpikeStrs[precision]] = []

    # Loop Over Files ------------------------------------------------------------- #

    for i in idxs:

        j = int(i) - 1 # for new data (no i = '1')
                
        # 1x and 4x data to compare (for AGx)
        datapath_1x = ih.datapath('1x', freq, amp  , sample, i )
        datapath_4x = ih.datapath('4x', freq, amp_ , sample, i )
        data1x      = ih.loadData( datapath_1x, i )[0]
        data4x      = ih.loadData( datapath_4x, i )[0]
        data4x      = ( data4x - offset4x)*scale_ + offset4x # shift and scale
        
        # Read in AG data
        datapath = ''
        try:
            datapath = ih.datapath(gain, freq, amp, sample, i )
        except:
            print "Data on bad data list or does not exist; skipping"
            continue

        if debug: print 'Reading in ' + datapath
        data        = ih.loadData( datapath, i )
        adc_counts  = data[0][:n_read]
        gain_bit    = data[1][:n_read]

        # AG
        SineWave_AG = AutogainSineWave(gain, freq, amp, sample, i, adc_counts, gain_bit, debug)
        
        # AGX
        AGx_counts   = ConstructAGx(data1x, data4x, gain_bit )
        SineWave_AGx = AutogainSineWave(gain, freq, amp, sample, i, AGx_counts, gain_bit, debug)

        # Set fit params (sine fits + residual fits)
        SineWave_AG.SetAutogainSineParams(  "sine", fit_params_1x, fit_params_4x )
        SineWave_AGx.SetAutogainSineParams( "sine", fit_params_1x, fit_params_4x )
        SineWave_AG.SetAutogainSineParams(  "res" , fit_params_res_1x, fit_params_res_4x)
        SineWave_AGx.SetAutogainSineParams( "res" , fit_params_res_1x, fit_params_res_4x)

        # BASELINE CORRECTION ----------------------------------------------------- #
        
        sineAG_baseline  = SineWave_AG.CorrectAG_Baseline()
        sineAGx_baseline = SineWave_AGx.CorrectAG_Baseline()

        # ------------------------------------------------------------------------- #
        
        if useAveragesForCalib:
            gain_bit_[j]         = gain_bit          # ARRAY
            adc_counts_[j]       = adc_counts        # ARRAY
            sineAG_baseline_[j]  = sineAG_baseline   # ARRAY
            sineAGx_baseline_[j] = sineAGx_baseline  # ARRAY

        # ADDITIONAL CORRECTIONS (Non-Averaged Sines) ----------------------------- #
        else: 
            
            # --------------------------------------------------------------------- #
            # RUN!
            enobs, spikeVals = ApplyCorrections( SineWave_AG, SineWave_AGx, enobs )

            for k in range(4):
                spikeVals_[k].append(spikeVals[k])
        # ------------------------------------------------------------------------- #

    # END LOOP OVER IDXS ------------------------------------------------------------------------------------------

    # ADDITIONAL CORRECTIONS (Averaged Sines) ------------------------------------- #
    if useAveragesForCalib:
        
        # Take Averages
        gain_bit_avg         = [ np.mean( np.transpose( gain_bit_ )[i]   ) for i in range(n_read) ]
        adc_counts_avg       = [ np.mean( np.transpose( adc_counts_ )[i] ) for i in range(n_read) ]
        sineAG_baseline_avg  = [ np.mean( np.transpose( sineAG_baseline_  )[i] ) for i in range(n_read) ]
        sineAGx_baseline_avg = [ np.mean( np.transpose( sineAGx_baseline_ )[i] ) for i in range(n_read) ]
        sineAG_baseline_std  = [ np.std( np.transpose( sineAG_baseline_  )[i] ) for i in range(n_read) ]
        sineAGx_baseline_std = [ np.std( np.transpose( sineAGx_baseline_ )[i] ) for i in range(n_read) ]

        # Construct Autogain sine wave objects, assign values & fill with averaged data; ignore adc_counts
        SineWave_AG_all  = AutogainSineWave(gain, freq, amp, sample, '0', adc_counts_avg, gain_bit_avg, debug, sineAG_baseline_avg)
        SineWave_AGx_all = AutogainSineWave(gain, freq, amp, sample, '0', adc_counts_avg, gain_bit_avg, debug, sineAGx_baseline_avg)

        SineWave_AG_all.SetAutogainSineParams(  "sine", fit_params_1x, fit_params_4x )
        SineWave_AGx_all.SetAutogainSineParams( "sine", fit_params_1x, fit_params_4x )
        SineWave_AG_all.SetAutogainSineParams(  "res" , fit_params_res_1x, fit_params_res_4x)
        SineWave_AGx_all.SetAutogainSineParams( "res" , fit_params_res_1x, fit_params_res_4x)

        # ------------------------------------------------------------------------- #
        # RUN! 
        enobs, spikeVals = ApplyCorrections( SineWave_AG_all, SineWave_AGx_all, enobs )

        # Unpack Spikes
        pos01_, pos10_, neg01_, neg10_ = spikeVals
        spikesAvg = [ np.mean(pos01_), np.mean(pos10_), np.mean(neg01_), np.mean(neg10_) ]
        spikesStd = [ np.std(pos01_) , np.std(pos10_) , np.std(neg01_) , np.std(neg10_)  ]

        if PLOT_STDEV:
            nplot = int(4/float( fit_params_1x[0])) + 50 # 4 full sines + some more
            x = np.arange(nplot)
            plt.clf()
            plt.plot(x, sineAG_baseline_std[:nplot]  , 'k.', label='AG')
            plt.plot(x, sineAGx_baseline_std[:nplot] , 'g.', label='AGx')
            plt.title('Standard Dev of points'), plt.ylabel('ADC Counts'), plt.xlabel('Sample')
            plt.legend(loc='upper right', fancybox=True, fontsize='10')
            plotpath_ = ph.plotpath('../results/CalibrateAG/StandardDeviations/Stdev'+avg_str, gain, freq, amp, sample, '0', compare='index')
            plt.savefig( plotpath_ )
            print 'Plot saved to', plotpath_

    else:

        # Unpack Values
        for k in range(4):
            spikeVals_[k] = [item for sublist in spikeVals_[k] for item in sublist]
        
        spikesAvg = [ np.mean( np.transpose( spikeVals_ )[i] ) for i in range(4) ]
        spikesStd = [ np.mean( np.transpose( spikeVals_ )[i] ) for i in range(4) ]

    # -------------------------------------------------------------------------------------------------------------
    
    print '-----------------------------------------------------------------'
    print 'ENOB RESULTS:', gain, freq, amp
    enobs_toprint = ['spikeU', 'AG'] + ['spikeC_'+SpikeStrs[precision] for precision in SpikeStrs]
    enob_raw = np.mean(enobs['AG'])
    for enob_type in enobs_toprint:
        enob_ = np.mean(enobs[enob_type])
        print enob_type.upper(), '     \t=\t', enob_, ' ( +', round(enob_ - enob_raw, 4),')'
    print '-----------------------------------------------------------------'
    
    return enobs, spikesAvg, spikesStd

#==================================================================================================================
def ApplyCorrections( SineWave_AG, SineWave_AGx, enobs ): 
    """This function applies the various corrections to a given sine wave."""
    
    if SineWave_AG.debug:
        print "Function: ApplyCorrections()"

    gain, freq, amp, sample, index = SineWave_AG.gain, SineWave_AG.freq, SineWave_AG.amp, SineWave_AG.sample, SineWave_AG.index

    SineWave_AG.FitSine()
    SineWave_AGx.FitSine()

    # Get ENOBs
    enobs['AG'].append( SineWave_AG.CalculateENOB() )
    enobs['AGx'].append( SineWave_AGx.CalculateENOB() )

    # Spike Corrections ----------------------------------------------------------- #
    
    # 0.01 bit precesion
    precesion = '0.01'
    SineWave_SpikeCorr_0p01 = SineWave_AG.CorrectAG_Spikes(method='C', c01=SpikeValues[precesion][0], c10=SpikeValues[precesion][1] )
    enobs['spikeC_'+SpikeStrs[precesion]].append( SineWave_SpikeCorr_0p01.CalculateENOB() )
    if PLOT_SPIKE: MultiPlot( SineWave_AG, SineWave_SpikeCorr_0p01, "Spike", plotpath_ext='C_'+SpikeStrs[precesion])

    # 0.1 bit precesion
    precesion = '0.1'
    SineWave_SpikeCorr_0p1  = SineWave_AG.CorrectAG_Spikes(method='C', c01=SpikeValues[precesion][0], c10=SpikeValues[precesion][1] )
    enobs['spikeC_'+SpikeStrs[precesion]].append( SineWave_SpikeCorr_0p1.CalculateENOB() )
    if PLOT_SPIKE: MultiPlot( SineWave_AG, SineWave_SpikeCorr_0p1, "Spike", plotpath_ext='C_'+SpikeStrs[precesion])

    # 0.5 bit precesion
    precesion = '0.5'
    SineWave_SpikeCorr_0p5 = SineWave_AG.CorrectAG_Spikes(method='C', c01=SpikeValues[precesion][0], c10=SpikeValues[precesion][1] )
    enobs['spikeC_'+SpikeStrs[precesion]].append( SineWave_SpikeCorr_0p5.CalculateENOB() )
    if PLOT_SPIKE: MultiPlot( SineWave_AG, SineWave_SpikeCorr_0p5, "Spike", plotpath_ext='C_'+SpikeStrs[precesion])

    # 1 bit precision
    precesion = '1'
    SineWave_SpikeCorr_1p = SineWave_AG.CorrectAG_Spikes(method='C', c01=SpikeValues[precesion][0], c10=SpikeValues[precesion][1] )
    enobs['spikeC_'+SpikeStrs[precesion]].append( SineWave_SpikeCorr_1p.CalculateENOB() )
    if PLOT_SPIKE: MultiPlot( SineWave_AG, SineWave_SpikeCorr_1p, "Spike", plotpath_ext='C_'+SpikeStrs[precesion])

    # Spike -- Unique, Dynamic
    SineWave_SpikeCorr_U = SineWave_AG.CorrectAG_Spikes(method='ZD')
    enobs['spikeU'].append( SineWave_SpikeCorr_U.CalculateENOB() )
    if PLOT_SPIKE: MultiPlot( SineWave_AG, SineWave_SpikeCorr_U, "Spike", plotpath_ext='ZD')
    spikeVals = SineWave_SpikeCorr_U.spike_values # save spike values using this method

    # Other Corrections ----------------------------------------------------------- #

    # Distortion Prediction 
    SineWave_DistCorr = SineWave_AG.CorrectAG_Distortion()
    #enobs['dist_ag'].append( SineWave_DistCorr.CalculateENOB() )
    if PLOT_DIST: MultiPlot( SineWave_AG, SineWave_DistCorr, "Distortion")

    # Distortion Correction for AGx
    SineWave_DistCorr_AGx = SineWave_AGx.CorrectAG_Distortion()
    #enobs['dist_agx'].append( SineWave_DistCorr_AGx.CalculateENOB() )
    if PLOT_DIST: MultiPlot( SineWave_AGx, SineWave_DistCorr_AGx, "Distortion", plotpath_ext='AGx')

    # Phase
    SineWave_PhaseCorr = SineWave_AG.CorrectAG_Phase()
    #enobs['phase'].append( SineWave_PhaseCorr.CalculateENOB() )
    if PLOT_PHASE: MultiPlot( SineWave_AG, SineWave_PhaseCorr, "Phase")

    # More options -- layering corrections (out-of-date)
    # * Dist -> Spikes (2 corrections)
    # * Dist -> Spikes -> Phase (3 corrections)

    # ----------------------------------------------------------------------------- #
    if PLOT_FFT:

        pf.plotFFT(SineWave_AG,          plotstyle='b.-', legend='AG')
        pf.plotFFT(SineWave_SpikeCorr_U, plotstyle='r.-', legend='Spike Corr')
        pf.plotFFT(SineWave_AGx,         plotstyle='g.-', legend='AGx')
    
        plt.title('FFT'), plt.xlabel('Frequency (MHz)'), plt.ylabel('ADC Counts')
        plt.legend(loc='upper right', fancybox=True, fontsize='10')

        plotpath_ = ph.plotpath('../results/CalibrateAG/FFTs/AG_FFT'+avg_str, gain, freq, amp, sample, index, compare='index')
        plt.savefig( plotpath_ )
        print 'Plot saved to', plotpath_
    # ----------------------------------------------------------------------------- #

    if PLOT_MAIN is False:
        return enobs, spikeVals 

    # Plotting ----------------------------------------------------------------------------------------------------

    nplot = int(2.5*float(sample)/float(freq.replace('p','.'))) + 50 # 2.5 full pulses + some more
    x = np.arange(nplot)

    nfine = nplot*10
    xfine = np.arange(nfine*2,step=float(nplot)/float(nfine))

    # Funcs to plot
    sine = SineWave_AG.sine[:nplot]
    res  = sine[:nplot] - SineWave_AG.fitted_sine[:nplot]
    fitted_sine_fine = sf.mySine(xfine, *SineWave_AG.fit_params)
    gain_bit_scaled  = [ SineWave_AG.gain_bit[i]*(max(sine) - min(sine)) + min(sine) for i in range(nplot) ]

    #--------------------------------------------------------------------------------------------------------------
    plt.clf()
    plt.figure(1)
            
    plt.subplot(211)
    plt.title('Autogain First Correction: '+ph.plotInfo(gain, freq, amp, sample,index,compare='index') )
    plt.ylabel('ADC Counts, 14b scale')
    plt.plot(x, gain_bit_scaled, 'k.-', label='Gain Bit (scaled)')
    plt.plot(x, sine           , 'b.' , label='AG Sine (baseline), ENOB='+str(round(SineWave_AG.enob, 2)) )
    plt.plot(xfine[:nfine], fitted_sine_fine[:nfine], 'r-', label='Fitted Sine')
    plt.legend(loc='upper right', fancybox=True, fontsize='10')
            
    plt.subplot(212)
    plt.ylabel('ADC Counts'), plt.xlabel('Samples')
    plt.plot(x, res[:nplot], 'b.-', label='Fit Residual')
    plt.legend(loc='upper right', fancybox=True, fontsize='10')

    plotpath_ = ph.plotpath('../results/CalibrateAG/MainPlots/CalibrateAG'+avg_str, gain, freq, amp, sample, index, compare='index')
    plt.savefig( plotpath_ )
    print 'Plot saved to', plotpath_
    #plt.show()
    #--------------------------------------------------------------------------------------------------------------
            
    return enobs, spikeVals

# =================================================================================================================
def ConstructAGx(data1x, data4x, gain_bit):
    """Function to "simulate" AG data from 1x and 4x samples. Looks like AG Raw ADC Counts. Must be corrected with
    correctAG_raw(...). """

    AGx_counts = [ data1x[i]*gain_bit[i] + data4x[i]*(1-gain_bit[i]) for i in range(len(gain_bit)) ]

    return AGx_counts
        
# =================================================================================================================
def MultiPlot( MySineWave, MyCorrectedSineWave, plot_type, plotpath_ext=''):
    """Function to plot autogain-related corrections
    plot_type options: Spike, Distortion, Phase
    """
    if MySineWave.debug:
        print "Function: MultiPlot()"

    plot_type_options = ["Spike", "Distortion", "Phase"]
    if plot_type not in plot_type_options:
        print "WARNING: plot type", plot_type, "not in available options:", plot_type_options

    if MySineWave.fit_params is None:
        MySineWave.fitSine()

    Nplot       = int(4/float( MySineWave.fit_params[0])) + 50 # 4 full sines + some more
    sine        = MySineWave.sine
    fitted_sine = MySineWave.fitted_sine
    corrected_sine = MyCorrectedSineWave.sine

    # Plotting #
    diff         = [ sine[i] - fitted_sine[i] for i in range(Nplot) ]
    diff_corr    = [ corrected_sine[i] - fitted_sine[i] for i in range(Nplot) ]
    gain_bit     = MySineWave.gain_bit[:Nplot]

    scale = 1.2*(max(diff) - min(diff))/(max(sine) - min(sine))
    shift = np.mean(sine) 
    scaled_sine  = [ ( sine[i] - shift )*scale for i in range(Nplot) ]

    distortion_correction = []
    if plot_type == "Distortion":
        distortion_correction = [MyCorrectedSineWave.sine[i] - MySineWave.sine[i] for i in range(Nplot)]

    x = np.arange(Nplot)

    plt.clf()
    plt.plot(x, scaled_sine, 'g.-', label='Baseline AG Sine, Scaled')
    plt.plot(x, diff, 'b.-', label='AG Sine - Fitted Sine')
    if plot_type != "Distortion":
        plt.plot(x, diff_corr, 'r-', label='AG Corrected Sine - Fitted Sine')
    else: 
        plt.plot(x, distortion_correction, 'r-', linewidth=1, label='Distortion Correction')
    plt.plot(x, gain_bit, 'k.-', label='Gain Bit')

    plt.title('Correction: '+plot_type)
    plt.legend(loc='upper right', fancybox=True, fontsize='10')
    plt.xlabel('Sample Number'), plt.ylabel('ADC Counts')

    gain, freq, amp, index = MySineWave.gain, MySineWave.freq, MySineWave.amp, MySineWave.index
    if plotpath_ext != '': plotpath_ext += '_'
    plotpath = '../results/CalibrateAG/All/'+plot_type+'/'+plot_type+'_freq'+freq+'_amp'+amp+'_'+gain+'_'+plotpath_ext+index+'.png'

    plt.savefig(plotpath)
    plt.clf()
    print 'Plot saved to:', plotpath

#==================================================================================================================
if __name__ == "__main__":
    main()
