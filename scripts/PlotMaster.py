###################################################################################################################
#
#  This is the Plotting Master Script. 
#   - Command Line Arguments : use -h
#   - Inputs : .npy or .hdf5 sine wave files
#   - Output : plots
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#  Updated: May 2019
#
###################################################################################################################

import sys
import math
import numpy as np
from scipy.fftpack import fft
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import gridspec

# Local Modules
sys.path.append("../src/")
import InputHelper as ih
import PlotHelper as ph
import PlottingFunctions as pf
import SineFunctions as sf
from SineWave import SineWave

#import ROOT
#ROOT.PyConfig.IgnoreCommandLineOptions = True
#ROOT.gROOT.SetBatch(1)
#from ROOT import THStack, TH1F, TF1, TCanvas, gStyle, TLatex, TColor, TLegend, TGraph, TGraphErrors, TVector, TVectorT, gPad

dataDir = '../data/v3/'

#------------------------------------------------------------------------------------------------------------------
def main():

    plotting_options = ['Fit'     ,  # plotFittedSine(...)
                        'Residual',  # plotSineResidual(...)
                        'Checkfit',  # checkSineFit(...)
                        'FFT'     ]  # plotFFT(...)
                        
    # Parse
    args = ih.parseMe( sys.argv )
    inputfile, plottype, gain, freq, amp, sample, idx, compare, doCalib = args.file, args.plottype, args.gain, args.freq, args.amp, args.sample, args.idx, args.compare, args.doCalib
    #sample, plottype, inputfile,compare = args.sample, args.plottype, args.input, args.compare
    debug = args.debug

    if plottype not in plotting_options:
        print( 'ERROR: plottype [-p][--plottype] not in plotting options.')
        print( '       Plotting options are:', plotting_options)
        return

    #if compare == 'freq' and plottype in ['Residual', 'Fit']: 
    #    print 'WARNING: option to compare frequencies does not work well with [-p] [--plottype] = "'+str(plottype)+'"'

    # Read In Saved Data (for Checkfit option)
    sinedata_path = '../data/SineParams/SineParams_latest.npy' # latest is default

    # Loop
    loop_grid = ih.loopGrid( gain, freq, amp, idx, compare )
    #loop_grid = ih.loopGrid(1,1,1,1,'')
    print('loop grid: ' , loop_grid)
    
    i_loop = 0
    for (g, f, a, i) in loop_grid:

        # Read in data
        #datapath = ih.datapath(g, f, a, sample, i)
        #datapath = '../data/v3/20191223_cv3tb_sine_0p986MHz_32bitMode_ch8.txt'
        datapath = dataDir + inputfile
        #datapath = '../data/v3/20191222_sinewave_normalMode_john.txt'
        if datapath is None:
          print('Datapath', datapath, 'does not exist. Exiting')
          return

        print('Reading in ' + datapath)
        num_lines = 0
        with open(datapath, 'r') as f:
          for line in f:
            num_lines += 1
        print("Number of lines:")
        print(num_lines)
	
	#data         = ih.loadData( datapath, i )
	#gain_bit     = data[1][:4096]
        if 'MDAC' in datapath: adc_counts,gain_arr  = ih.ReadMDACBits(datapath,num_lines, doCalib)
        elif 'normal' in datapath: adc_counts = ih.convertBinaryToDecimal(datapath)
        else: adc_counts = ih.Read32ModeBits(datapath, num_lines)
        
        gain_bit     = np.zeros(num_lines)
        nsamples     = len(adc_counts)
        
        #print('adc counts: ', adc_counts, ', gain: ' , gain_bit)
        #print('Gain arr vals: ' , gain_arr)
        mdac_corrs = [4492.25, 4490.25, 4517.0, 4526.0, 4506.5, 4472.0] #Julia derived
        mdac_corrs = [4500.25, 4476.5, 4477.25, 4479, 4479.25, 4477.75, 4481.25, 4510.75] #Brian derived
        mdac_corrs = [4902.75, 4940.75, 4902.25, 4872.5, 4888.5, 4905.25, 4904.0, 4889.25]  #rough new board calibration
        mdac_corrs = [4900, 4900, 4900, 4900, 4900, 4900, 4900, 4900]  #rough new board calibration

        #do correction for linear fit 
        if doCalib: 
          #adc_counts_corr = []
          adc_counts_corr = adc_counts
          for index in range(len(adc_counts)): #MDAC bit 7, no data so far
            if gain_arr[index] == 0: adc_counts_corr[index] = (adc_counts[index] + sum(mdac_corrs[0:8])) #MDAC bit 7
            if gain_arr[index] == 1: adc_counts_corr[index] = (adc_counts[index] + sum(mdac_corrs[0:7])) #MDAC bit 6
            if gain_arr[index] == 2: adc_counts_corr[index] = (adc_counts[index] + sum(mdac_corrs[0:6])) #MDAC bit 6
            if gain_arr[index] == 3: adc_counts_corr[index] = (adc_counts[index] + sum(mdac_corrs[0:5])) #MDAC bit 5
            if gain_arr[index] == 4: adc_counts_corr[index] = (adc_counts[index] + sum(mdac_corrs[0:4]))
            if gain_arr[index] == 5: adc_counts_corr[index] = (adc_counts[index] + sum(mdac_corrs[0:3]))
            if gain_arr[index] == 6: adc_counts_corr[index] = (adc_counts[index] + sum(mdac_corrs[0:2]))
            if gain_arr[index] == 7: adc_counts_corr[index] = (adc_counts[index] + sum(mdac_corrs[0:1])) #just first correction
            if gain_arr[index] == 8: adc_counts_corr[index] = adc_counts[index]  # no correction
            #if gain_arr[index] == 8: adc_counts_corr[index] = adc_counts[index] #MDAC bit 0, no correction
 


          SineWave_tmp = SineWave(g, f, a, sample, i,adc_counts_corr[1:], gain_bit, debug)

        else: SineWave_tmp = SineWave(g, f, a, sample, i,adc_counts[1:], gain_bit, debug)
        MySineWaveResidual = SineWave_tmp.GetSineWaveResidual() 
        if doCalib: pf.plotResidualHists( MySineWaveResidual, gain_arr, datapath) 
 
        # Deal with autogain
        #if g == 'AG':
        
        # Plot style
        plotstyle      = ph.plotStyle(i_loop)
        plotstyle_sym  = ph.plotStyle(i_loop, linestyle='symbols')
        plotstyle_line = ph.plotStyle(i_loop, linestyle='lines')
        plotlegend     = ph.plotLegend(g, f, a, sample, i, compare)
  
        
        # Plot Fitted Sine --------------------------------------------------------------------------------------------------
        if plottype == 'Fit':
            pf.plotFittedSine( SineWave_tmp, doCalculation=True, datastyle=plotstyle_sym, fitstyle=plotstyle_line, legend=plotlegend+' FitInfo' )
        
        # Plot Sine Residual ------------------------------------------------------------------------------------------------
        if plottype == 'Residual':
            pf.plotSineResiduals( SineWave_tmp, datastyle=plotstyle_sym, fitstyle=plotstyle_line,
                                  legend1=plotlegend+' freq', legend2=plotlegend+' freq')
        
        # Check Sine Fit ----------------------------------------------------------------------------------------------------
        if plottype == 'Checkfit':
        
            # Load Parameters
            SineWave_tmp.LoadSineData( sinedata_path )
            
            pf.checkSineFit( SineWave_tmp )
        
            # Post Processing Here
            plotpath = ph.plotpath('../results/CheckSineFits/checksinefit', g, f, a, sample, i, compare='')
            plottitle = 'Check Sine Fit: '+ ph.plotInfo( g, f, a, sample, i, compare='' )
        
            plt.subplot(211)
            plt.title( plottitle )
            plt.ylabel('ADC Counts [Sine]')
            plt.legend(loc='best', fancybox=True, fontsize='10')
        
            plt.subplot(212)
            plt.xlabel('Sample Number')
            plt.ylabel('ADC Counts [Residual]')
            plt.legend(loc='best', fancybox=True, fontsize='10')
        
            plt.savefig( plotpath )
            print('Plot saved to', plotpath)
            plt.clf()
        
        # Plot FFT ----------------------------------------------------------------------------------------------------------
        if plottype == 'FFT':
            pf.plotFFT( SineWave_tmp, plotstyle=plotstyle, legend=plotlegend )
        
        i_loop += 1

    # END OF LOOP #

    if plottype == 'Checkfit':
        return
    
    # Format Output
    plotpath = ''
    plotinfo = ph.plotInfo(  gain, freq, amp, sample, idx, compare )
    
    if plottype == 'Fit': # -------------------------------------------------------------------------------------------------
        plotpath = ph.plotpath('../results/SineFits/sinefit', gain, freq, amp, sample, idx, compare)
        plottitle = 'Sine Fit: '+ plotinfo

        plt.title( plottitle )
        plt.xlabel('Sample Number')
        plt.ylabel('ADC Counts')
        plt.legend(loc='best', fancybox=True, title=compare.title(), fontsize='10')
        
    if plottype == 'Residual': # --------------------------------------------------------------------------------------------
        plotpath = ph.plotpath('../results/Residuals/sineresidual', gain, freq, amp, sample, idx, compare)
        plottitle = 'Sine Residual and Fit: '+ plotinfo

        plt.subplot(211)
        plt.ylabel('Sine [ADC Counts]')
        plt.title( plottitle )
        plt.legend(loc='best', fancybox=True, title=compare.title(), fontsize='10')
        
        plt.subplot(212)
        plt.ylabel('Residual [ADC Counts]')
        plt.xlabel('Sample Number')
        plt.legend(loc='best', fancybox=True, title=compare.title(), fontsize='10')
        
    if plottype == 'FFT': # -------------------------------------------------------------------------------------------------
        plotpath = ph.plotpath('../results/FFTs/fft', gain, freq, amp, sample, idx, compare)
        plottitle = 'FFT: '+ plotinfo

        plt.title( plottitle )
        plt.xlabel('Frequency [MHz]')
        plt.ylabel('Signal Strength [dB]')
        plt.legend(loc='best', fancybox=True, title=compare.title(), fontsize='10')
        
    if args.output is not None:
        plotpath = args.output

    plt.savefig( plotpath )
    print('Plot saved to', plotpath)
    plt.clf()
    
    return

#----------------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    main()
