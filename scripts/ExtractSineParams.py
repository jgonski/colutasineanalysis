###################################################################################################################
#
#  This script extracts various sine performance and fit parameters. 
#   - Command Line Arguments : None [ See Options Box ]
#   - Inputs : .npy sine wave files
#   - Output : A dictionary with various sine wave parameters stored (used as input in other scripts)
#
#  Author: Kiley Kennedy <kk3107@columbia.edu>, <kiley.elizabeth.kennedy@cern.ch>
#  Date: March 2019
#  Updated: May 2019
#
###################################################################################################################

import datetime
import math
import numpy as np
from scipy.fftpack import fft

import os
import sys
sys.path.append("../src/")

# Local Modules
import InputHelper as ih
import SineFunctions as sf
from SineWave import SineWave

# OPTIONS -------------------------
debug = False
sample = '40'
useAveragedSines = False
doResidualFit = True

# Sine Fit Quality Control
sineThreshold = 0.05
resThreshold = 2.5

#------------------------------------------------------------------------------------------------------------------
def stats( array ):
    """Returns various statistical properties of an input array"""
    avg = np.mean( array )
    std = np.std( array )

    return array # Choose to return all the information
    # return avg, std # OR Choose to return only the avg and stdev

#------------------------------------------------------------------------------------------------------------------
def distRatio( fft ):
    """Get the ratio of the third harmonic over the fundamental"""
    n = len(fft)
    f_fund = np.argmax(abs( fft[5:n/2] ) ) + 5

    if f_fund > 4096/3: # third harmonic above nyquist (ignore for now)
        return 0.0 
        
    a_fund = abs( fft[f_fund] )
    a_dist   = max(abs( fft[f_fund*3-10:f_fund*3+10] ) ) 

    return float(a_dist/a_fund)

#------------------------------------------------------------------------------------------------------------------
def main():

    # Name the output
    now = datetime.datetime.now().strftime('%Y-%m-%d_%Hh%M')
    output = '../data/SineParams/SineParams_'+now+'.npy'
    output_latest = '../data/SineParams/SineParams_latest.npy'
        
    freqs = ih.validFreqs() 	
    amps  = ih.validAmps()
    gains = ['1x', '4x' ] #ih.validGains()
    idxs  = ih.validIdxs()

    n_read = 4096

    sinePerformanceVals = {} #initialize dict

    loop_grid = [(g, f, a) for g in gains for f in freqs for a in amps ]
    for (g, f, a) in loop_grid:
        
        # Don't read in things that don't exist 
        if g == '4x' and ih.myFloat(a) > 0.25: continue

	# Initialize arrays -- for averaging and stdevs
	freq_ = []
        amp_ = []
	phase_ = [] 
	offset_ = [] 
	chi2_ = []
	enob_ = []
	sfdr_ = []
	snr_  = []

    	# investigte distortion/Residual
	freqRes_ = []
	ampRes_ = []
	phaseRes_ = []
	offsetRes_ = []

        # Ratio Distortion/Fundamental
	amplRatDF_ = []

	# Distorion amplitude by hand
	amplRes_byHand_ = []

	# corrected
	freqC_ = []
	ampC_ = []
	phaseC_ = []
	offsetC_ = []
	enobC_ = []

	# Did we collect data?
	dataOK = False

	for i in idxs:

            if useAveragedSines: i = '0'
                        
	    datapath_ = ih.datapath(g, f, a, sample, i)
	    if datapath_ is None: continue
	    dataOK = True

	    if debug: print '----- Reading in ' + datapath_ + ' -----'

	    data = ih.loadData( datapath_, i )
	    adc_counts = data[0][:n_read] 
	    gain_bit   = data[1][:n_read]

	    # ----- Fit Sine ----- #
	    
            SineWave_tmp = SineWave(g, f, a, sample, i ,adc_counts, gain_bit, debug)
	    SineWave_tmp.max_diff_cutoff = sineThreshold
            SineWave_tmp.FitSine()
    
            if not SineWave_tmp.fit_isOK: 
                print 'ERROR: bad fit (', SineWave_tmp.fit_max_diff,') -- OMITTING', datapath_
                if debug:
                    print ' -- NOTE: If you see this error, you should probably check how the fit is working,'
                    print '          or change the threshold (as long as the fit is still ok)'
                continue # omit bad fits

            
            # ----- Fit Residual ----- #
            
            SineWaveResidual_tmp = SineWave_tmp.GetSineWaveResidual()
            SineWaveResidual_tmp.max_diff_cutoff = resThreshold
            SineWaveResidual_tmp.FitSine()
            
            if not SineWaveResidual_tmp.fit_isOK:
                SineWaveResidual_tmp.fitted_sine = np.zeros( SineWaveResidual_tmp.N )              
                SineWaveResidual_tmp.fit_params  = [0.0, 0.0, 0.0, 0.0]
                print 'ERROR: bad residual fit (', SineWaveResidual_tmp.fit_max_diff,') -- NOT OMITTING', datapath_
                if debug:
                    print ' -- NOTE: If you see this error, you should probably check how the fit is working,'
                    print '          or change the threshold (as long as the fit is still ok)'
                
            # ----- Get corrected values ----- #
            
            SineWave_tmp.SineWaveResidual.fitted_sine = SineWaveResidual_tmp.fitted_sine # reset
            SineWave_tmp.SineWaveResidual.fit_params  = SineWaveResidual_tmp.fit_params  # reset

            SineWaveCorrected_tmp = SineWave_tmp.GetSineWaveCorrected()
            SineWaveCorrected_tmp.max_diff_cutoff = sineThreshold
            SineWaveCorrected_tmp.FitSine()
            
            if not SineWaveCorrected_tmp.fit_isOK: 
                print 'ERROR: bad fit (', SineWaveCorrected_tmp.fit_max_diff,') -- OMITTING', datapath_
                if debug:
                    print ' -- NOTE: If you see this error, you should probably check how the fit is working,'
                    print '          or change the threshold (as long as the fit is still ok)'
                continue # omit bad fits
                                
	    # ----- Get ADC Performance and Fit Values ----- #

	    SineWave_tmp.CalculatePerformanceParams()
	    SineWaveCorrected_tmp.CalculatePerformanceParams()

	    enob_val   = SineWave_tmp.enob
	    sfdr_val   = SineWave_tmp.sfdr
	    snr_val    = SineWave_tmp.snr
	    enobC_val  = SineWaveCorrected_tmp.enob

	    fit_params      = SineWave_tmp.fit_params
            fit_params_res  = SineWaveResidual_tmp.fit_params
	    fit_params_corr = SineWaveCorrected_tmp.fit_params

	    # ----- Append arrays to store ----- #

	    # Original
	    freq_.append( fit_params[0] ) # *sampling_frequency to get MHz
            amp_.append( fit_params[1] )
            phase_.append( sf.fixPhase(fit_params[2]) ) # fix the phase here
            offset_.append( fit_params[3] )
            chi2_.append( sf.chi2(SineWave_tmp.sine,SineWave_tmp.fitted_sine) )

            enob_.append( enob_val )
            sfdr_.append( sfdr_val )
	    snr_.append( snr_val )
			
	    # Residual
            freqRes_.append( fit_params_res[0] ) # *sampling_frequency to get MHz
            ampRes_.append( fit_params_res[1] )
            phaseRes_.append( sf.fixPhase(fit_params_res[2]) )
            offsetRes_.append( fit_params_res[3] )

            # Corrected
            freqC_.append( fit_params_corr[0] ) # *sampling_frequency to get MHz
            ampC_.append( fit_params_corr[1] )
            phaseC_.append( sf.fixPhase(fit_params_corr[2]) )
            offsetC_.append( fit_params_corr[3] )
            enobC_.append( enobC_val)

            # ----- Other ----- #
            
            # Distortion Amplitude Ratio
            amplRatDF_.append( distRatio( SineWaveCorrected_tmp.fft ) )

            # General Distortion 
            amplRes_byHand_.append( (max(SineWaveResidual_tmp.sine) - min(SineWaveResidual_tmp.sine))*.5 )
                        
            if useAveragedSines: break

        # Don't fill if no data read in
	if dataOK == False: continue
		
        print 'Ending', g, f, a, '-- N saved =', len( freq_ )
	sinePerformanceVals[(g, f, a)] = { # Fit Params
                                            'freq': stats(freq_), 
                                            'amp': stats(amp_),
                                            'phase': stats(phase_),
                                            'offset': stats(offset_),
                                            'chi2': stats(chi2_),
                                            # Fit Residuals
                                            'freqR': stats(freqRes_), 
					    'ampR': stats(ampRes_),
                                            'phaseR': stats(phaseRes_),
                                            'offsetR': stats(offsetRes_),
                                            # Performance Params
                                            'enob': stats(enob_), 
                                            'sfdr': stats(sfdr_),  
                                            'snr' : stats(snr_) ,
                                            # Corrected Params (residual-corrected)
                                            'freqC': stats(freqC_), 
					    'ampC': stats(ampC_),
                                            'phaseC': stats(phaseC_),
                                            'offsetC': stats(offsetC_),
                                            'enobC': stats(enobC_),
                                            # Other Params
                                            'amplRatDist': stats(amplRatDF_),
                                            'amplRHand' : stats(amplRes_byHand_), 
                                            }

    
    np.save( output , sinePerformanceVals )
    print 'Data saved to', output

    np.save( output_latest , sinePerformanceVals )
    print 'Data also saved to', output_latest

#------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
	main()	
