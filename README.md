# Coluta Sine Analysis

### Setup

Clone the respository and checkout a new branch 
```
git clone ssh://git@gitlab.cern.ch:7999/kikenned/ColutaSineAnalysis.git
cd ColutaSineAnalysis
git checkout -b <your_branch_name>
```

Alternatively, you can copy it directly on xenia (currently on branch "freshstart")
```
# Copy it to your working space on xenia
cp -r /data/users/kkennedy/LArPhaseII/COLUTAAnalysis/ColutaSineAnalysis .

# Or, if you want to do it locally on your own computer:
scp -r yourname@xenia.nevis.columbia.edu:/data/users/kkennedy/LArPhaseII/COLUTAAnalysis/ColutaSineAnalysis .

cd ColutaSineAnalysis
```

Finally, make sure you have the correct directory structure and python version requirements
```
# Setup the directory structure
sh setup.sh

# Setup the necessary python requirements (optional, see more below). Be careful with python versions!
pip install <package_name>==<version> --user
```

### Python Packages

Python packages and versions used in repository development:
* python 2.7.10
* argparse==1.1
* numpy==1.16.1
* matplotlib=1.3.1
* scipy==0.13.0b1
* h5py==2.9.0
* math
* sys
* datetime
* os

Note: this analysis package is compatible with other versions of these things, but at least, these ones do work. 

### Datasets

The repository comes with a mini dataset with the following:
*  All three gains: 1x, 4x, AG
*  3 Frequencies: 1, 5, and 13 MHz
*  2 Amplitudes: 0.22 and 0.88 V inputs
*  3 regular sine waves per (gain, frequency, amplitude), and 1 file containing the averaged sine waves
*  Note that there is no (4x, *MHz, 0.88 V) input file, because a sine wave that big would saturate the at 4x gain

You can also copy over a larger dataset in .npy format (optional):
```
# Remove existing dataset
cd data/npy/
rm -rf *  # be careful with -rf option! 

# Copy over new dataset
cp -r /data/users/kkennedy/LArPhaseII/COLUTAAnalysis/data_npy_feb2019/* .

# Copy over new config files
cd ../../config/
cp /data/users/kkennedy/LArPhaseII/COLUTAAnalysis/data_npy_feb2019/40MSPS/config/* . 
cd ../
```

# Tutorial

Read through the following examples, and copy the python commands to exectute the scripts with the given options. Once you execute the script, use the display command to open and view the resulting plot:
`display path/to/plot.png` (the script outputs will tell you the path to the plot!)

Go into the run directory:
`cd run`

## Part I: PlotMaster.py

### Example 0
Print the help message for PlotMaster.py (shows options and examples)

`python ../scripts/PlotMaster.py -h`

### Example 1
Plot a single sine wave and its fit. Here, we're plotting the sine wave with gain=1x, 
frequency=1 MHz, amplitude=0.88 Volts (input amplitude here, not amplitude in adc 
counts), and fileindex=5 (there are multiple sine wave files with the same (gain, freq, 
amp).

`python ../scripts/PlotMaster.py -p Fit -g 1x -f 1 -a 0p88 -i 3`

### Example 2
Plot a single sine wave FFT. 

a) Here, we're plotting the sine wave with gain=4x, freq=5 MHz, amplitude=0.18 V, and 
fileindex=2.

`python ../scripts/PlotMaster.py -p FFT -g 4x -f 5 -a 0p22 -i 2`

b) Here, we're plotting same but with fileindex=1 (if you do not specify an option with 
-i, index=1 is the default).

`python ../scripts/PlotMaster.py -p FFT -g 4x -f 5 -a 0p22`

c) Here, if you try plotting something with 4x gain but with an amplitude too large for 
4x, you'll get an error.

`python ../scripts/PlotMaster.py -p FFT -g 4x -f 5 -a 0p88`

d) You'll also get an error for invalid arguments:

`python ../scripts/PlotMaster.py -p HELLO -g TRY -f THIS -a PLEASE -i 99`

### Example 3
Sine fit residual: plot the sine wave and fit; then calculate the "fit residual", which 
is the (sine wave data) - (sine wave fit). If the residual contains a sine wave of 
another frequency (ie, there is a superposition of sine waves in the sample), then there 
might be a considerable amount of distortion in the signal. Studying the residual allows 
us to try to understand the source of the distortion. You might get some warnings about 
the fit not being ok; this if fine, it probably means that the script cannot find a good 
fit to the residual, meaning distortion is low!

a) Plot the sine wave residual with index=1 (default)

`python ../scripts/PlotMaster.py -p Residual -f 5 -a 0p88 -g 1x`

b) Using the option index=0 automatically plots the average of all sine waves for a given 
(gain, freq, amp) combination. The sine wave averages have been previously calculated and 
are stored in their own separate file (you'll see this when the file is read in).

`python ../scripts/PlotMaster.py -p Residual -f 5 -a 0p88 -g 1x -i 0`

### Example 4
Plot results from multiple different inputs in the same figure using the -c (--compare) 
option in PlotMaster.py. 

a) Plot multiple sine wave FFTs in the same figure. The following command plots the FFT 
for all inputs with: gain=1x, amp=0.88 V. In other words, it *compares* the frequencies.

`python ../scripts/PlotMaster.py -p FFT -g 1x -a 0p22 -c freq`

b) Plot multiple sine wave and their fits in the same figure. The following command plots 
the sine wave and fit for all inputs with gain=4x, freq=0.3 MHz. In other words, it 
*compares* the amplitudes. (Note: Depending on the dataset you're using, you might notice that the script tries to read in a 
file on the "bad data list", which is defined in src/InputHelper.py in the function 
isDataGood(). This file is omitted). 

`python ../scripts/PlotMaster.py -p Fit -g 1x -f 13 -c amp`

c) Compare option special. The 'special' option plots 1x and 4x sine waves that, after 
the gain is applied, has approximately the same amplitudes in adc counts. So in input 
amplitude of the 1x (in V) should be four times that of the 4x imput (in V). In order to 
choose the 'special' option, you must also specify the amplitude and gain combination 
which you would like to compare. Here, Plot 2 overlayed FFTs with freq=1 MHz: the first 
has gain=1x, amp=0.72 V; the second has gain=4x, amp=0.18 V. The following 2 commands are 
equivalent in terms of the plots they produce:

`python ../scripts/PlotMaster.py -p Residual -f 1 -g 1x -a 0p88 -c special`

`python ../scripts/PlotMaster.py -p Residual -f 1 -g 4x -a 0p22 -c special`

d) NOTE: You also have options to plot and compare the sampling frequency (option -s or 
--sample). The default sampling frequency is 40 MHz. 


## Part II: ExtractSineParams.py and PlotSineParams.py

### Example 5
Calculate and save various sine wave performance and fit parameters for *all* 1x and 4x 
sine waves in the dataset. These are automatically saved in the following files:
```
../data/SineParams_${date_and_time}.npy
../data/SineParams_latest.npy
```
The sine performance params are later read in to other scripts so that we don't have to 
calculate then over and over again. In the .npy file, they are saved as arrays of all 
values (ie, 1 value for each file index), so that we can later plot their distributions 
should we want to. 

`python ../scripts/ExtractSineParams.py`

### Example 6
Plot the sine performance params that you just calculated. 

a) Plot parameters from all sine waves. This will plot the averages of each parameter in 
its own figure, with standard deviations as error bars. Use the most recent .npy file 
generated by ExtractSineParams.py. (You can also try changing the first argument to a 
different .npy file)

`python ../scripts/PlotSineParams.py ../data/SineParams/SineParams_latest.npy`

b) Open ../scripts/PlotSineParams.py: 

`vi ../scripts/PlotSineParams.py` *OR* `emacs ../scripts/PlotSineParams.py`

Edit the following options in the "Options" Box:
```
special = False    
plotAll = True     
plotHists = True  
```
This will plot distributions (in histograms) of all of the sine wave parameters saved in 
SineParams_latest.npy. 

`python ../scripts/PlotSineParams.py ../data/SineParams/SineParams_latest.npy`


## Part III: Return to PlotMaster.py ###

### Example 7
You can check the sine wave fit parameters generated by ExtractSineParams.py. This 
functionality allows you to check the fit by eye.

a) To check a single fit:

`python ../scripts/PlotMaster.py -p Checkfit -g 1x -f 1 -a 0p22`

b) Try using a compare option:

`python ../scripts/PlotMaster.py -p Checkfit -g 1x -f 1 -c amp`


## PART IV: CalibrateAG.py

### Example 8
Run the autogain calibration for a single amplitude and frequency (gain=AG). This 
generates A TON of plots, and you don't have to look at all of them. The purpose of these
plots is to help diagnose issues with the calibration, or help you visualize what the 
various autogain corrections are axctually doing. You can turn off the options to produce
all of these plots (see Example 8). 

`python ../scripts/CalibrateAG.py -g AG -f 5 -a 0p88`

### Example 9
Try playing with some of the options in this script (under "GLOBAL OPTIONS"). This allows 
you to change various plotting options. Under "PLOTTING OPTIONS", set the following:
```
PLOT_DIST   = False
PLOT_SPIKE  = False
PLOT_PHASE  = False
PLOT_FFT    = False
PLOT_STDEV  = False
PLOT_MAIN   = False
PLOTS_FINAL = True
```

Now, running the autogain calibration for a multiple amplitudes and frequencys produces 
plots that compare various performance parameters (eg. ENOB):

a) Compare frequencies

`python ../scripts/CalibrateAG.py -g AG -a 0p88  -c freq`

b) Compare frequencies

`python ../scripts/CalibrateAG.py -g AG -f 1 -c amp`

Now, look at the plots in ../results/ENOBs/

### Example 10
You can also run the autogain calibration over *averaged* sine waves 
(useAveragesForCalib=True)
Or, you can run the autogain calibration over each sutogain sine wave 
(useAveragesForCalib=False)

Try playing with some of the global options in CalibrateAG.py, as well as the various 
possible input options (ie, frequency and amplitude).

## Part V: StudySpikes.py (in development) 


## Part VI: Advanced 

### Bash scripts in run/ for quick execution

If you want to run a lot of things at once, or save certain commands that you use often, you can write them in a bash script in the run/ directory. For example, make a new bash script run_calibrate_ag.sh paste the following text into it:
```
python CalibrateAG.py -g AG -a 0p22 -c freq
python CalibrateAG.py -g AG -a 0p88 -c freq
python CalibrateAG.py -g AG -c amp -f 1
python CalibrateAG.py -g AG -c amp -f 5
python CalibrateAG.py -g AG -c amp -f 13
```
Run it:
`sh run_calibrate_ag.sh`

### Config Files and Input Files

In ../config/ , you'll find config files listing the various frequencies, amplitudes, and gains in your dataset. You can change these as new datasets with different parameters become available.

For example, in InputFileType.txt, you can read in .npy files or .hdf5 files. 

NOTE: The path structure of the data in the data/ directory is important to reading in the 
files. So as data becomes available, make sure to save the data in a directory stucture 
that is consistent with how the data is read in (in src/InputHelper.py: datapath() 
function), or change how the data is read in in the scripts themselves. 

# Documentation

Documentation of past projects using this repository can be found in the doc/ directory

Archived respository here: https://gitlab.cern.ch/kikenned/ColutaSineAnalysis_Archive
